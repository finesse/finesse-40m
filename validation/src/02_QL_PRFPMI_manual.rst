.. include:: /defs.hrst
.. _examples_02_ql_fpmi:

40m Quantum Limited Sensitivity - PRFPMI
----------------------------------------------------

In this example, we check that that we demonstrate how to put the 40m model in the
PRFPMI state, but we don't use any locking actions.

We start with our required imports:

.. jupyter-execute::

    import finesse
    import finesse.analysis.actions as fac
    from finesse.detectors import PowerDetector
    import finesse.components as fc
    import numpy as np

    from finesse_ligo.actions import DRFPMI_state

    from finesse_40m.factory import FortyMeterFactory

    import matplotlib.pyplot as plt
    finesse.init_plotting()
    import pprint

We then build the model and apply the PRFPMI state.

.. jupyter-execute::

    factory = FortyMeterFactory()
    factory.reset()
    factory.options.LSC.add_DOFs = False # Allow independantly controlling PRM position

    # Set manual values like this
    #factory.params.Y.ITM.sus_Q_pos = 100
    #factory.params.Y.ITM.sus_f0_pos = 6
    factory.options.suspensions.test_masses = True

    # Print out build options
    pprint.PrettyPrinter(indent=4, sort_dicts=True).pprint(
        factory.options.toDict()
    )

    # Build model
    model = factory.make()
    model.modes(maxtem=4)  # basic maxtem

    model.run(DRFPMI_state('PRFPMI'))
    assert model.SRM.misaligned == True

We then manually define the operating point and check that the model has reached it.
This is checking:
  * The nodes are connected correctly.
  * The mode matching is good (see maxtem=4 above).
  * The IFO is at the correct operating point.

.. jupyter-execute::

    # Bring cavities onto resonance
    dc_offset = 125e-6 # degrees
    model.ITMY.phi=0 - dc_offset
    model.ETMY.phi=0 - dc_offset
    model.PRM.phi = 0

    # Get pds
    PDs = [
        detector for detector in model.detectors if isinstance(detector, PowerDetector)
    ]

    sim = model.run(fac.Noxaxis())
    for detector in PDs:
        print(f"{detector} = {sim[detector]:.1e} W")

    assert 3700 < sim['Px'] < 3800
    assert 3700 < sim['Py'] < 3800
    assert 14 < sim['Pinx'] < 15
    assert 14 < sim['Piny'] < 15
    assert 29 < sim['Pprc'] < 30
    assert sim['Psrc'] < 1

For completeness, we print out the tunings so you can compare your model against
this one.

.. jupyter-execute::

    def print_tunings(modelin, ARS=False):
        comps = list(modelin.components)

        comps.sort(key=lambda comp: comp.name)

        if not ARS:
            comps = [comp for comp in comps if comp.name[-2:] != "AR"]

        for comp in comps:
            if isinstance(comp, (fc.Mirror, fc.Beamsplitter)):
                print(comp.name, comp.phi)

    print_tunings(model)

We can now plot some cavity scans for reference.

.. jupyter-execute::

    ax1 = fac.Xaxis(model.L0.f, 'lin', -1.1 * model.cavXARM.FSR / 2, 1.1 * model.cavXARM.FSR / 2, 1000)
    ax2 = fac.Xaxis(model.PRM.phi, 'lin', -95, 95, 1000)

The full arm FSR:

.. jupyter-execute::

    out = model.run(ax1, progress_bar=True)
    out.plot(*PDs, logy=True)

The PRC FSR:

.. jupyter-execute::

    out = model.run(ax2, progress_bar=True)
    out.plot(*PDs, logy=True)

For completeness, the quantum-noise limited response of the 40m.

.. jupyter-execute::

    model2 = model.deepcopy()
    model2.parse("""
    # Differentially modulate the arm lengths
    fsig(1)
    sgen darmx LX.h
    sgen darmy LY.h phase=180

    # Output the full quantum noise limited sensitivity
    qnoised NSR_with_RP SRM.p2.o nsr=True
    # Output just the shot noise limited sensitivity
    qshot NSR_without_RP SRM.p2.o nsr=True

    # We could also display the quantum noise and the signal
    # separately by uncommenting these two lines.
    # qnoised noise srm.p2.o
    # pd1 signal srm.p2.o f=fsig
    """)

    ax = fac.Xaxis(model2.darmx.f, 'log', 0.3, 100e3, 300, name='LF')
    out = model2.run(ax,progress_bar=True)

    fig, ax1 = plt.subplots(ncols=1,sharey=True,figsize=(7,3))
    ax1.loglog(out.x0,2*out['NSR_with_RP'], c='r', ls='--')

    ax1.set_ylabel('Sensitivity [1/sqrt(Hz)]')

Recall the factor 2 from :ref:`examples_01_ql_simple_mich`.
TAG_BEGIN_TEST_ONLY

.. jupyter-execute::

    assert 1e-23 < np.abs(2*out['NSR_with_RP'][150]) < 1e-17

TAG_END_TEST_ONLY
