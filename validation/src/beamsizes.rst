.. include:: /defs.hrst
.. _examples_beamsize:

Beam Tracing
===============
In this example, we show the various beam profiles predicted by the model.

.. contents:: :local:

First we must set up the model. We will use a simple model with no detectors.

.. jupyter-execute::

    from finesse_40m.factory import FortyMeterFactory

    # Import the factory with default parameters
    factory = FortyMeterFactory()
    factory.options.add_detectors = False

    # Make a finesse model
    model = factory.make()

Mismatches Table
------------------
Finesse computes the optimal eigenmode at each point in the simulation based on the
inputs we give it. The Finesse-40m package has already defined cavities for the major
subsystems. Here we plot the points where the beam scatters between higher order modes
due to a Beam Parameter mismatch.

The mismatches here have been converted from float (0-1) into a percentage where
100% means the overlap integral between the two gaussian modes at each node is 0.

.. jupyter-execute::

    model.mismatches_table(numfmt='{:.2%}')


XARM
-------
This is the XARM eigenmode.

.. jupyter-execute::

    from finesse_40m.factory import FortyMeterFactory

    # Import the factory with default parameters
    factory = FortyMeterFactory()
    factory.options.add_detectors = False

    # Make a finesse model
    model = factory.make()

    ps = model.propagate_beam(model.ETMX.p1,model.ITMXAR.p2)
    ps.plot(resolution='all')
    print(ps)

YARM
-------
This is the YARM eigenmode.

.. jupyter-execute::

    ps = model.propagate_beam(model.ETMY.p1,model.ITMYAR.p2)
    ps.plot(resolution='all')
    print(ps)

Input Path
-------------
We can now take the YARM eigenmode and project it into the REFL port

.. jupyter-execute::

    ps = model.propagate_beam(model.ITMY.p2.i,model.REFL_port.p1.i)
    ps.plot(resolution='all')
    ps.print()

AS Path
-------------
We can now take the XARM eigenmode and project it into the AS port

.. jupyter-execute::

    ps = model.propagate_beam(model.ITMX.p2.i,model.AS_port.p1.i)
    ps.plot(resolution='all')
    ps.print()


POP Path
-------------
We can now take the XARM eigenmode and project it into the AS port

.. jupyter-execute::

    ps = model.propagate_beam(model.ITMX.p2.i,model.AS_port.p1.i)
    ps.plot(resolution='all')
    ps.print()


LO Path
-------------
We can now take the PRC eigenmode and project it along the LO to the BHD

.. jupyter-execute::

    ps = model.propagate_beam(model.PRM.p1.o,model.BHDBS_OUT2_Window.p1.i, via_node=model.LO1.p1.i)
    ps.plot(resolution='all')
    ps.print()


Output Path
-------------
We can now take the PRC eigenmode and project it along the LO to the BHD

.. jupyter-execute::

    ps = model.propagate_beam(model.ITMX.p1.i,model.BHDBS_OUT2_Window.p1.i, via_node=model.SRM.p1.i)
    ps.plot(resolution='all')
    ps.print()
