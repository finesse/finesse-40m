# `finesse-40m`


Finesse 3.0 LIGO models, tools, and data for the 40m. This package is an optional extra to
the `finesse-ligo` and `finesse` packages which must be installed to use this package.

This package is under active development and is not ready for general use.

## License
All code here is distributed under GPL v3.

## Documentation
Please see https://finesse.docs.ligo.org/finesse-40m/

## Packaging

The `finesse-40m` is automatically uploaded to pypi when new tags are pushed to `main`. Tags must be annotated and be in the semantic versioning form `MAJOR.MINOR.PATCH`:

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.

Only maintainers can push tags to the main branch.
