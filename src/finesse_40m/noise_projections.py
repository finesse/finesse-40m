import numpy as np
from scipy.interpolate import interp1d
import finesse.analysis.actions as fac


def get_short_michelson_response(
    model_in,
    freq_low=0.3,
    freq_high=1e4,
    npoints=1000,
    interpolate=True,
    interpolate_kw_args={},
):
    """Get the frequency response of a short Michelson interferometer.

    This function computes the response of a short Michelson interferometer by
    applying differential motion to the input test masses (ITMX and ITMY) and
    measuring the resulting power at the anti-symmetric (AS) port. The AS port
    is assumed to be represented in the Finesse model by a `Nothing` called
    "AS_port". The function creates a deep copy of the input model to ensure
    no modifications are made to the original.

    Parameters
    ----------
    model_in : finesse.Model
        The Finesse model representing the optical setup of the short Michelson
        interferometer. This model should include definitions for the test
        masses ITMX and ITMY, and a "Nothing" component named "AS_port".
    freq_low : float, optional
        The lower bound of the frequency range (in Hz) for the response calculation.
        Default is 0.3 Hz.
    freq_high : float, optional
        The upper bound of the frequency range (in Hz) for the response calculation.
        Default is 10,000 Hz.
    npoints : int, optional
        The number of frequency points to compute the response over the specified
        range. Default is 1000.
    interpolate : bool, optional
        Whether to return an interpolated function of the response. If `True`,
        the function returns a `scipy.interpolate.interp1d` object for efficient
        querying of the response. Default is `True`.
    interpolate_kw_args : dict, optional
        Additional keyword arguments to pass to the `scipy.interpolate.interp1d`
        function. For example, `fill_value` can be specified to handle out-of-bound
        queries. Default is an empty dictionary.

    Returns
    -------
    scipy.interpolate.interp1d or tuple
        If `interpolate` is `True`, returns a `scipy.interpolate.interp1d` object
        that provides the interpolated response as a function of frequency.
        If `interpolate` is `False`, returns a tuple `(frequencies, responses)`
        where:
        - `frequencies` is a numpy array of frequency points (in Hz).
        - `responses` is a numpy array of the response magnitudes (in Watts).

    Notes
    -----
    - The function generates a deep copy of the input `model_in` to ensure the
      original model remains unchanged.
    - A Finesse script is parsed into the copied model to configure the differential
      drive of ITMX and ITMY and set up the power detection at the AS port.
    - The response is computed as the power at the AS port normalized by 0.5 to
      account for the differential drive amplitudes.
    - Interpolation is provided for convenience when precise frequency queries
      are required outside the original sampled frequencies.
    """

    model = model_in.deepcopy()
    model.parse(
        """
    # Differentially modulate the arm lengths
    fsig(1)
    sgen darmx ITMX.mech.z
    sgen darmy ITMY.mech.z phase=180

    pd2 signal AS_port.p1 f2 0 fsig
    """
    )

    ax = fac.Xaxis(model.darmx.f, "log", 0.3, 100e3, 100, name="LF")
    qm_noise = model.run(ax, progress_bar=True)

    if interpolate:
        if "fill_value" not in interpolate_kw_args:
            interpolate_kw_args["fill_value"] = 0
        if "bounds_error" not in interpolate_kw_args:
            interpolate_kw_args["bounds_error"] = False

        f_response = interp1d(
            qm_noise.x0, 0.5 * np.abs(qm_noise["signal"]), **interpolate_kw_args
        )
        return f_response
    else:
        return qm_noise.x0, 0.5 * np.abs(qm_noise["signal"])


def get_short_michelson_quantum_noise(
    model_in,
    freq_low=0.3,
    freq_high=1e4,
    npoints=1000,
    interpolate=True,
    interpolate_kw_args={},
):
    """Get the frequency quantum noise for a short Michelson interferometer.

    This function computes the quantum noise of a short Michelson interferometer by
    applying differential motion to the input test masses (ITMX and ITMY) and
    measuring the resulting power at the anti-symmetric (AS) port. The AS port
    is assumed to be represented in the Finesse model by a `Nothing` called
    "AS_port". The function creates a deep copy of the input model to ensure
    no modifications are made to the original.

    Parameters
    ----------
    model_in : finesse.Model
        The Finesse model representing the optical setup of the short Michelson
        interferometer. This model should include definitions for the test
        masses ITMX and ITMY, and a "Nothing" component named "AS_port".
    freq_low : float, optional
        The lower bound of the frequency range (in Hz) for the response calculation.
        Default is 0.3 Hz.
    freq_high : float, optional
        The upper bound of the frequency range (in Hz) for the response calculation.
        Default is 10,000 Hz.
    npoints : int, optional
        The number of frequency points to compute the response over the specified
        range. Default is 1000.
    interpolate : bool, optional
        Whether to return an interpolated function of the response. If `True`,
        the function returns a `scipy.interpolate.interp1d` object for efficient
        querying of the response. Default is `True`.
    interpolate_kw_args : dict, optional
        Additional keyword arguments to pass to the `scipy.interpolate.interp1d`
        function. For example, `fill_value` can be specified to handle out-of-bound
        queries. Default is an empty dictionary.

    Returns
    -------
    scipy.interpolate.interp1d or tuple
        If `interpolate` is `True`, returns a `scipy.interpolate.interp1d` object
        that provides the interpolated response as a function of frequency.
        If `interpolate` is `False`, returns a tuple `(frequencies, responses)`
        where:
        - `frequencies` is a numpy array of frequency points (in Hz).
        - `responses` is a numpy array of the response magnitudes (in Watts).

    Notes
    -----
    - The function generates a deep copy of the input `model_in` to ensure the
      original model remains unchanged.
    - A Finesse script is parsed into the copied model to configure the differential
      drive of ITMX and ITMY and set up the power detection at the AS port.
    - Interpolation is provided for convenience when precise frequency queries
      are required outside the original sampled frequencies.
    """

    model = model_in.deepcopy()
    model.parse(
        """
    # Differentially modulate the arm lengths
    fsig(1)
    sgen darmx ITMX.mech.z
    sgen darmy ITMY.mech.z phase=180

    qnoised1 noise_with_RP AS_port.p1 f2 0 nsr=False
    """
    )

    ax = fac.Xaxis(model.darmx.f, "log", freq_low, freq_high, npoints, name="LF")
    qm_noise = model.run(ax, progress_bar=True)

    if interpolate:
        if "fill_value" not in interpolate_kw_args:
            interpolate_kw_args["fill_value"] = 0
        if "bounds_error" not in interpolate_kw_args:
            interpolate_kw_args["bounds_error"] = False

        f_response = interp1d(
            qm_noise.x0, np.abs(qm_noise["noise_with_RP"]), **interpolate_kw_args
        )
        return f_response
    else:
        return qm_noise.x0, np.abs(qm_noise["noise_with_RP"])
