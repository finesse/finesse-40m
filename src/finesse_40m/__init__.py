from .logging import get_logger

# Set up logger for module_a
logger = get_logger()

from . import factory

__all__ = (factory,)
