import finesse
import finesse.components as fc
from .base import FortyMeterFactory


class FortyMeterFPMI(FortyMeterFactory):
    def set_default_options(self):
        """Resets factory to all default options."""

        super().set_default_options()

        self.options.INPUT.add_IMC_and_IM1 = False
        self.options.INPUT.set_IMC_mode = False

    def make(self):
        base = finesse.Model()
        self.pre_make(base, self.params)

        self.add_arm_cavity(base, "X", self.params.X, self.options)
        self.add_arm_cavity(base, "Y", self.params.Y, self.options)
        self.add_BS(base, self.params.BS)
        self.add_MICH(base, "X", self.params.X, self.BS_X, self.ITMX_AR_fr)
        self.add_MICH(base, "Y", self.params.Y, self.BS_Y, self.ITMY_AR_fr)

        self.add_input_path(base, self.params.INPUT, self.BS_PR)

        # Add cavities wont work so we just manually add them
        base.add(fc.Cavity("cavXARM", base.ETMX.p1.o, priority=100))
        base.add(fc.Cavity("cavYARM", base.ETMY.p1.o, priority=100))

        base.beam_trace()
        return base
