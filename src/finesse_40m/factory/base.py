# Python Standard Library
import importlib.resources
import os

# Pure Python Tools
from munch import Munch

# SciPy Stack
import numpy as np

# Finesse Imports
import finesse
import finesse.components as fc
from finesse.detectors import PowerDetector, AmplitudeDetector, MathDetector
from finesse.symbols import Constant

# from finesse.symbols import Constant
from finesse.materials import FusedSilica

# import finesse.analysis.actions as fa

# Finesse LIGO Imports
from finesse_ligo.factory.base import DRFPMIFactory, update_mapping

from ..logging import get_logger

# Set up logger for module_a
logger = get_logger()


class FortyMeterFactory(DRFPMIFactory):
    def __init__(self, *args, **kwargs):
        if len(args) == 0 and len(kwargs) == 0:
            args = (
                importlib.resources.files("finesse_40m.parameter_files").joinpath(
                    "C1.yaml"
                ),
            )

        super().__init__(*args, **kwargs)

    def update_parameters(self, parameters):
        """Update the parameters.

        Parameters
        ----------
        parameters : MunchDict | str
            Munch dict of parameters, or a filename and path
        """
        if isinstance(parameters, Munch):
            update_mapping(self.params, parameters)

        elif os.path.exists(parameters):
            with open(parameters) as f:
                update_mapping(self.params, Munch.fromYAML(f.read()))

        elif importlib.resources.is_resource(
            "finesse_ligo.parameter_files", parameters
        ):
            update_mapping(
                self.params,
                Munch.fromYAML(
                    importlib.resources.read_text(
                        "finesse_ligo.parameter_files", parameters
                    )
                ),
            )
        else:
            raise Exception(f"Could not handle parameters: {parameters}")

    def reset(self):
        """Resets factory to all default options, drives, and LSC/ASC settings."""
        self.set_default_options()
        self.set_default_drives()
        self.set_default_LSC()
        # self.set_default_ASC()

    def set_max_reality(self):
        """Max reality models the following things:
          - Suspensions using ZPK
          - Optics under the paraxial approximation, up to specified maxtem
          - 11, 33, 165 MHz sidebands up to order 3
          - AR coatings on all transmissive optics
          - AR coatings on the ETMs
          - "Thick" beamsplitter

        The following things are not yet modelled (and can be enabled / added, but are not tested)
          - Wedges
          - Control loops

        You still need to set an appropriate maxtem!"""
        self.options = Munch()
        self.options.BS_type = "thick"
        self.options.add_11MHz = True
        self.options.add_33MHz = True
        self.options.add_55MHz = True
        self.options.add_165MHz = True

        self.options.ETMAR = True

        self.options.suspensions.test_masses = True
        self.options.suspensions.core_optics = True
        self.options.suspensions.auxillary_optics = True

    def set_default_options(self):
        """Resets factory to all default options.

        This function defines the following options:
            self.options.BS_type: "thick", "thin", "fake"
            self.options.add_11MHz: bool
            self.options.add_33MHz: bool
            self.options.add_55MHz: bool
            self.options.add_165MHz: bool
            self.options.modulation_order: int

            self.options.wedges: bool
            self.options.ETMAR: bool
            self.options.PR2_PR3_substrates: bool

            self.options.suspensions.test_masses: bool
            self.options.suspensions.core_optics: bool
            self.options.suspensions.auxillary_optics: bool

            self.options.INPUT.add_IMC_and_IM1: bool
            self.options.INPUT.set_IMC_mode: bool

            self.options.OUTPUT.readout: "AS", "HD", "BHD" (HD & BHD are not implemented)

            self.options.LSC.add_DOFs: bool
            self.options.LSC.add_readouts: bool
            self.options.LSC.add_output_detectors: bool
            self.options.LSC.close_AC_loops = bool

        The following options are defined in finesse-ligo but are not yet
        implemented in finesse-40 and must remain False.

            self.options.LSC.add_locks = bool
            self.options.LSC.add_AC_loops = bool

        Additionally the following options differ in function to finesse-ligo

            self.options.add_detectors: bool, power, amplitude
            self.options.materials.test_mass_substrate: finesse.materials.Material

        You can inspect the current values by printing the options dictionary.
        """

        super().set_default_options()

        # self.options defined in DRFPMIFactory
        # self.options.INPUT defined in DRFPMIFactory
        # self.options.LSC defined in DRFPMIFactory

        for item in ["add_9MHz", "add_45MHz", "add_118MHz"]:
            self.options.pop(item)

        self.options.BS_type = "thick"
        self.options.add_11MHz = True
        self.options.add_33MHz = True
        self.options.add_55MHz = True
        self.options.add_165MHz = False
        self.options.modulation_order = 3

        self.options.wedges = False
        self.options.ETMAR = False
        self.options.PR2_PR3_substrates = True

        for item in ["QUAD_suspension_kwargs", "QUAD_suspension_model"]:
            self.options.pop(item)

        self.options.suspensions = Munch()
        self.options.suspensions.test_masses = False
        self.options.suspensions.core_optics = False
        self.options.suspensions.auxillary_optics = False

        self.options.INPUT.add_IMC_and_IM1 = False
        self.options.INPUT.set_IMC_mode = True

        self.options.OUTPUT = Munch()
        self.options.OUTPUT.readout = "AS"

        self.options.LSC.add_DOFs = True
        self.options.LSC.add_readouts = True
        self.options.LSC.add_output_detectors = False
        self.options.LSC.close_AC_loops = False

        # These are not used and must remain false
        self.options.LSC.add_locks = False
        self.options.LSC.add_AC_loops = False

        self.options.pop("ASC")

    def add_PDs(self):
        output_dets = self.options.add_detectors
        if isinstance(output_dets, str):
            if output_dets.lower() in ["power", "pds", "pd", "pdonly", "true"]:
                return True
            else:
                return False
        else:
            return output_dets
        raise ValueError("`add_detectors` may be True, False, `power` or `amplitude`")

    def add_ADs(self):
        output_dets = self.options.add_detectors
        if isinstance(output_dets, str):
            if output_dets.lower() in ["amplitude", "ads", "ad", "adonly", "true"]:
                return True
            else:
                return False
        else:
            return output_dets
        raise ValueError("`add_detectors` may be True, False, `power` or `amplitude`")

    def pre_make(self, model, params):
        if self.options.wedges:
            logger.warning(
                "Wedges are untested! Please check the paraxial "
                "approximation is valid at all points"
            )

    def make(self):
        base = finesse.Model()

        self.pre_make(base, self.params)

        self.add_arm_cavity(base, "X", self.params.X, self.options)
        self.add_arm_cavity(base, "Y", self.params.Y, self.options)
        self.add_BS(base, self.params.BS)
        self.add_MICH(base, "X", self.params.X, self.BS_X, self.ITMX_AR_fr)
        self.add_MICH(base, "Y", self.params.Y, self.BS_Y, self.ITMY_AR_fr)

        if self.options.get("fake_prc_gouy", False):
            raise NotImplementedError("Fake PRC is not confgured for 40m models")

        self.add_PRC(base, self.params.PRC, self.BS_PR, self.options)
        self.add_SRC(base, self.params.SRC, self.BS_SR, self.options)

        self.add_input_path(base, self.params.INPUT, self.PRM_AR_fr)
        self.add_output_path(base, self.params.AS, self.SRM_AR_fr)
        self.add_LO_path(base, self.params.BH)
        self.add_POP_telescopes(base, self.params.POP)

        self.add_cavities(base)

        self.add_suspensions(base, self.params)
        self.add_LSC(base)
        # if self.options.ASC.add:
        #     self.add_ASC(base)

        if self.add_ADs() or self.add_PDs():
            self.add_detectors(base)

        # if self.options.apertures.add:
        #     self.add_apertures(base)
        # if self.options.thermal.add:
        #     self.add_test_mass_thermal(base)
        # if self.options.add_transmon:
        #     self.add_transmon(base)

        # self.post_make(base, self.params)
        base.beam_trace()
        return base

    def add_arm_cavity(self, model, which, params, options):
        # Based on DRFPI.add_arm_cavity but slightly different to allow us to tweek things
        # Note DRFPI.add_arm_cavity puts all loss on ETM, we don't do that

        ITM = model.add(
            fc.Mirror(
                f"ITM{which}",
                T=params.ITM.HR.T,
                L=params.ITM.HR.L,
                Rc=float(params.ITM.HR.Rc),
            )
        )

        ETM = model.add(
            fc.Mirror(
                f"ETM{which}",
                T=params.ETM.HR.T,
                L=params.ETM.HR.L,
                Rc=float(params.ETM.HR.Rc),
            )
        )

        ITMAR = model.add(
            fc.Mirror(
                f"ITM{which}AR",
                R=params.ITM.AR.R,
                L=params.ITM.AR.R,
                xbeta=ITM.xbeta.ref,
                ybeta=ITM.ybeta.ref,
                phi=ITM.phi.ref,
            )
        )
        if options.wedges:
            raise NotImplementedError

        if options.ETMAR:
            ETMAR = model.add(
                fc.Mirror(
                    f"ETM{which}AR",
                    R=1,
                    L=0,
                    xbeta=ITM.xbeta.ref,
                    ybeta=ITM.ybeta.ref,
                    phi=ITM.phi.ref,
                )
            )

            model.connect(
                ETMAR.p2,
                ITM.p2,
                name=f"subETM{which}",
                L=params.ETM.thickness,
                nr=self.options.materials.test_mass_substrate.nr,
            )
            self._link_all_mechanical_dofs(ETM, ETMAR)

        model.connect(ITM.p1, ETM.p1, name=f"L{which}", L=params.length_arm)
        model.connect(
            ITMAR.p2,
            ITM.p2,
            name=f"subITM{which}",
            L=params.ITM.thickness,
            nr=self.options.materials.test_mass_substrate.nr,
        )
        self._link_all_mechanical_dofs(ITM, ITMAR)

        if self.add_PDs():
            model.add(PowerDetector(f"P{which.lower()}", ETM.p1.o))
            model.add(PowerDetector(f"Pin{which.lower()}", ITMAR.p1.i))

        if self.add_ADs():
            model.add(
                AmplitudeDetector(
                    f"a_carrier_pin{which.lower()}",
                    ITM.p2.i,
                    f=0,
                )
            )
            model.add(
                AmplitudeDetector(
                    f"a_carrier_{which.lower()}",
                    ETM.p1.o,
                    f=0,
                )
            )
            model.add(
                AmplitudeDetector(
                    f"a_carrier_00_{which.lower()}",
                    ETM.p1.o,
                    f=0,
                    n=0,
                    m=0,
                )
            )

        setattr(self, f"ITM{which}_AR_fr", ITMAR.p1)
        setattr(self, f"ITM{which}_HR_bk", ITM.p2)

    # TODO: Backpropagate this to finesse-ligo
    def _link_all_mechanical_dofs(self, comp1, comp2, facing="auto"):
        """Connects signal flow for the mechanical motion of component 1 to component 2.
        Doesn't create a bi-directional connection.

        Parameters
        ----------
        comp1, comp2 : ModelElement
            Components to connect
        facing : Bool or 'auto'
            If 'auto' determine if the components are facing intelligently, this
            only works for mirrors, not other elements
        """
        if comp1._model is not comp2._model:
            raise RuntimeError(f"{comp1} and {comp2} are not part of the same model")

        model = comp1._model
        if facing == "auto":
            try:
                assert isinstance(comp1, fc.Mirror)
                assert isinstance(comp2, fc.Mirror)
            except AssertionError:
                raise NotImplementedError(
                    "facing=auto is only implemented for mirrors."
                    " Please specify the direction explicitly"
                )

            if (comp1.p1.attached_to == comp2.p1.attached_to) or (
                comp1.p2.attached_to == comp2.p2.attached_to
            ):
                # Surfaces pointing towards each other
                z_gain = -1
                # Pitch sign flips when surfaces pointing towards each other
                pitch_gain = -1
            elif (comp1.p1.attached_to == comp2.p2.attached_to) or (
                comp1.p2.attached_to == comp2.p1.attached_to
            ):
                # Surfaces pointing towards each other
                z_gain = +1
                pitch_gain = +1
            else:
                raise NotImplementedError()
        else:
            try:
                assert isinstance(facing, bool)
            except AssertionError:
                raise NotImplementedError('facing must be "auto", True or False')

            if facing:
                z_gain = -1
                pitch_gain = -1
            else:
                z_gain = 1
                pitch_gain = 1

        model.connect(comp1.mech.z, comp2.mech.z, gain=z_gain)
        model.connect(comp1.mech.pitch, comp2.mech.pitch, gain=pitch_gain)
        model.connect(comp1.mech.yaw, comp2.mech.yaw)  # yaw_gain always == 1

    def add_PRC(self, model, params, BS_port, options):
        PRM = model.add(
            fc.Mirror("PRM", T=params.PRM.T, L=params.PRM.L, Rc=params.PRM.Rc)
        )

        if hasattr(params.PRMAR, "T"):
            PRMAR_T = float(params.PRMAR.T)
        else:
            PRMAR_T = 1.0 - float(params.PRMAR.R) - float(params.PRMAR.L)

        PRMAR = model.add(
            fc.Mirror(
                "PRMAR",
                T=PRMAR_T,
                L=params.PRMAR.L,
                Rc=params.PRMAR.Rc,
                xbeta=PRM.xbeta.ref,
                ybeta=PRM.ybeta.ref,
                phi=PRM.phi.ref,
            )
        )

        PR2 = model.add(
            fc.Beamsplitter(
                "PR2",
                T=params.PR2.T,
                L=params.PR2.L,
                Rc=params.PR2.Rc,
                alpha=params.PR2.AOI,
            )
        )

        PR3 = model.add(
            fc.Beamsplitter(
                "PR3",
                T=params.PR3.T,
                L=params.PR3.L,
                Rc=params.PR3.Rc,
                alpha=params.PR3.AOI,
            )
        )

        model.connect(PRM.p1, PR2.p1, name="lp1", L=params.length_PRM_PR2)
        model.connect(PR2.p2, PR3.p1, name="lp2", L=params.length_PR2_PR3)
        model.connect(PR3.p2, BS_port, name="lp3", L=params.length_PR3_BS)

        if self.options.get("fake_prc_gouy", False):
            prc_gouy = model.add_parameter("PRC_gouy", 20)
            prc_gouy_astig = model.add_parameter("PRC_gouy_astig", 0)
            model.lp1.user_gouy_x = prc_gouy.ref + prc_gouy_astig.ref
            model.lp1.user_gouy_y = prc_gouy.ref - prc_gouy_astig.ref
            model.lp1.user_gouy_y = model.lp2.user_gouy_x = 0
            model.lp1.user_gouy_y = model.lp3.user_gouy_x = 0

        model.connect(
            PRMAR.p1, PRM.p2, name="subPRM", L=params.PRM.thickness, nr=FusedSilica.nr
        )
        self._link_all_mechanical_dofs(PRM, PRMAR)

        self.PRM_AR_fr = PRMAR.p2
        self.PRM_HR_fr = PRM.p1

        if options.PR2_PR3_substrates:
            if self.options.wedges:
                wedge = np.deg2rad(params.PR2AR.get("wedge", 0))
            else:
                wedge = 0
            PR2AR_for = model.add(
                fc.Mirror(
                    "PR2AR_for",
                    R=params.PR2AR.R,
                    L=params.PR2AR.L,
                    Rc=params.PR2AR.Rc,
                    xbeta=PR2.xbeta.ref + wedge,
                    ybeta=PR2.ybeta.ref,
                    phi=PR2.phi.ref,
                )
            )

            model.connect(
                PR2.p3,
                PR2AR_for.p1,
                name="subPR2_for",
                L=params.PR2.thickness,
                nr=FusedSilica.nr,
            )

            self._link_all_mechanical_dofs(PR2, PR2AR_for, facing=False)
            self.PR2_for_port = PR2AR_for.p2

            PR2AR_rev = model.add(
                fc.Mirror(
                    "PR2AR_rev",
                    R=params.PR2AR.R,
                    L=params.PR2AR.L,
                    Rc=params.PR2AR.Rc,
                    xbeta=PR2.xbeta.ref + wedge,
                    ybeta=PR2.ybeta.ref,
                    phi=PR2.phi.ref,
                )
            )

            model.connect(
                PR2.p4,
                PR2AR_rev.p1,
                name="subPR2_rev",
                L=params.PR2.thickness,
                nr=FusedSilica.nr,
            )
            self.PR2_rev_port = PR2AR_rev.p2
            self._link_all_mechanical_dofs(PR2, PR2AR_rev, facing=False)

            if self.options.wedges:
                wedge = np.deg2rad(params.PR3AR.get("wedge", 0))
            else:
                wedge = 0

            PR3AR = model.add(
                fc.Mirror(
                    "PR3AR",
                    R=params.PR3AR.R,
                    L=params.PR3AR.L,
                    Rc=params.PR3AR.Rc,
                    xbeta=PR3.xbeta.ref + wedge,
                    ybeta=PR3.ybeta.ref,
                    phi=PR3.phi.ref,
                )
            )

            model.connect(
                PR3.p3,
                PR3AR.p1,
                name="subPR3",
                L=params.PR3.thickness,
                nr=FusedSilica.nr,
            )
            self._link_all_mechanical_dofs(PR3, PR3AR, facing=False)
        else:
            POP_port = model.add(fc.Nothing("POP_port"))
            model.connect(PR2.p3, POP_port.p1)
            self.POP_port = POP_port.p1

        if options.add_detectors:
            model.add(PowerDetector("Pprc", model.PRM.p1.o))
            model.add(AmplitudeDetector("a_carrier_in", self.PRM_AR_fr.i, f=0))
            model.add(AmplitudeDetector("a_carrier_prc", model.PRM.p1.o, f=0))

    # TODO: This entire function is identical to finesse-ligo
    # aside from these two lines
    # self._link_all_mechanical_dofs(BS, BSARarm, facing=False)
    # self._link_all_mechanical_dofs(BS, BSARSR, facing=False)
    # Need to submit a PR for finesse-ligo and then delete this function
    def add_BS(self, model, params):
        BS = model.add(
            fc.Beamsplitter(
                "BS",
                T=params.T,
                L=params.L,
                alpha=params.AOI,
            )
        )

        BS_type = self.options.get("BS_type", "thick")
        if BS_type not in ["thick", "thin", "fake"]:
            raise ValueError(f"Unrecognized BS type: {BS_type}")

        self.options.BS_trans_arm = self.options.BS_trans_arm.upper()
        assert self.options.BS_trans_arm in ("X", "Y")
        trans_arm = self.options.BS_trans_arm

        if BS_type == "fake":
            if trans_arm == "X":
                self.BS_X = BS.p3
                self.BS_Y = BS.p2
            else:
                self.BS_X = BS.p2
                self.BS_Y = BS.p3
            self.BS_PR = BS.p1
            self.BS_SR = BS.p4
            self.BS_HR_X = BS.p3
            self.BS_HR_Y = BS.p2
        else:
            alpha_AR, L_BS_sub = self._BS_substrate_length(BS, params)
            if BS_type == "thin":
                L_BS_sub = 0
            BSARarm = model.add(
                fc.Beamsplitter(
                    f"BSAR{trans_arm}",
                    R=0,
                    L=params.R_AR,
                    alpha=np.rad2deg(alpha_AR),
                    phi=BS.phi.ref,
                    xbeta=BS.xbeta.ref,
                    ybeta=BS.ybeta.ref,
                )
            )
            BSARSR = model.add(
                fc.Beamsplitter(
                    "BSARAS",
                    R=0,
                    L=params.R_AR,
                    alpha=np.rad2deg(alpha_AR),
                    phi=BS.phi.ref,
                    xbeta=BS.xbeta.ref,
                    ybeta=BS.ybeta.ref,
                )
            )
            model.connect(
                BS.p3,
                BSARarm.p1,
                L=L_BS_sub,
                name=f"subBS_{trans_arm}",
                nr=FusedSilica.nr,
            )
            model.connect(
                BS.p4,
                BSARSR.p2,
                L=L_BS_sub,
                name="subBS_SR",
                nr=FusedSilica.nr,
            )
            self._link_all_mechanical_dofs(BS, BSARarm, facing=False)
            self._link_all_mechanical_dofs(BS, BSARSR, facing=False)

            if trans_arm == "X":
                self.BS_X = BSARarm.p3
                self.BS_Y = BS.p2
                self.BS_HR_X = BS.p3
                self.BS_HR_Y = BS.p2
            else:
                self.BS_X = BS.p2
                self.BS_Y = BSARarm.p3
                self.BS_HR_X = BS.p2
                self.BS_HR_Y = BS.p3

            self.BS_PR = BS.p1
            self.BS_SR = BSARSR.p4

    def add_MICH(self, model, which, params, BS_port, ITM_AR_fr_port):
        model.connect(
            BS_port, ITM_AR_fr_port, name=f"l{which.lower()}1", L=params.lenght_BS_TM
        )

        trans_arm = self.options.BS_trans_arm
        use_thin_BS = self.options.get("BS_type", "thick") in ["thin", "fake"]
        if use_thin_BS and which.upper() == trans_arm:
            # If we are using a thin BS then we need to take into account the lost path
            # length from using a thin BS to keep the same schnupp length
            _, L_BS_sub = self._BS_substrate_length(model.BS, self.params.BS)
            model.get(f"l{which.lower()}1").L += L_BS_sub * FusedSilica.nr

    def add_SRC(self, model, params, BS_port, options):
        SRM = model.add(
            fc.Mirror("SRM", T=params.SRM.T, L=params.SRM.L, Rc=params.SRM.Rc)
        )

        if hasattr(params.SRMAR, "T"):
            SRMAR_T = float(params.SRMAR.T)
        else:
            SRMAR_T = 1.0 - float(params.SRMAR.R) - float(params.SRMAR.L)

        if self.options.wedges:
            wedge = np.deg2rad(params.SRMAR.get("wedge", 0))
        else:
            wedge = 0
        SRMAR = model.add(
            fc.Mirror(
                "SRMAR",
                T=SRMAR_T,
                L=params.SRMAR.L,
                Rc=params.SRMAR.Rc,
                xbeta=SRM.xbeta.ref + wedge,
                ybeta=SRM.ybeta.ref,
                phi=SRM.phi.ref,
            )
        )

        SR2 = model.add(
            fc.Beamsplitter(
                "SR2",
                T=params.SR2.T,
                L=params.SR2.L,
                Rc=params.SR2.Rc,
                alpha=params.SR2.AOI,
            )
        )

        model.connect(
            SRMAR.p2, SRM.p2, name="subSRM", L=params.SRM.thickness, nr=FusedSilica.nr
        )
        model.connect(SRM.p1, SR2.p1, name="ls1", L=params.length_SRM_SR2)
        model.connect(SR2.p2, BS_port, name="ls2", L=params.length_SR2_BS)

        if self.options.get("fake_src_gouy", False):
            raise NotImplementedError("fake_src_gouy not implemented in 40m model")
        #     src_gouy = model.add_parameter("SRC_gouy", 20)
        #     src_gouy_astig = model.add_parameter("SRC_gouy_astig", 0)
        #     model.ls1.user_gouy_x = src_gouy.ref - src_gouy_astig.ref
        #     model.ls1.user_gouy_y = src_gouy.ref + src_gouy_astig.ref
        #     model.ls2.user_gouy_y = model.ls2.user_gouy_x = 0
        #     model.ls3.user_gouy_y = model.ls3.user_gouy_x = 0

        self._link_all_mechanical_dofs(SRM, SRMAR)

        if self.options.get("BS_type", "thick") in ["thin", "fake"]:
            # Adds extra optical path length to get correct SRC length if
            # using a thin BS
            _, L_BS_sub = self._BS_substrate_length(model.BS, self.params.BS)
            model.ls2.L += L_BS_sub * FusedSilica.nr

        self.SRM_AR_fr = SRMAR.p1
        self.SRM_HR_fr = SRM.p1

        if self.add_PDs():
            model.add(PowerDetector("Psrc", model.SRM.p1.o))

        if self.add_ADs():
            model.add(AmplitudeDetector("a_carrier_src", model.SRM.p1.i, f=0))

    def add_output_path(self, model, ASpars, SRM_AR_fr):
        AS1 = model.add(
            fc.Beamsplitter(
                "AS1",
                T=ASpars.AS1.HR.T,
                L=ASpars.AS1.HR.L,
                Rc=ASpars.AS1.HR.Rc,
                alpha=ASpars.AS1.HR.AOI,
            )
        )

        model.connect(SRM_AR_fr, AS1.p1, L=ASpars.length_SRMAR_AS1HR)

        AS2 = model.add(
            fc.Beamsplitter(
                "AS2",
                T=ASpars.AS2.HR.T,
                L=ASpars.AS2.HR.L,
                Rc=ASpars.AS2.HR.Rc,
                alpha=ASpars.AS2.HR.AOI,
            )
        )

        model.connect(AS1.p2, AS2.p1, L=ASpars.length_AS1HR_AS2HR)

        AS2AR = model.add(
            fc.Mirror(
                "AS2AR",
                R=ASpars.AS2.AR.R,
                L=ASpars.AS2.AR.L,
                xbeta=AS2.xbeta.ref,
                ybeta=AS2.ybeta.ref,
                phi=AS2.phi.ref,
            )
        )

        model.connect(
            AS2.p3, AS2AR.p2, name="subAS2", L=ASpars.AS2.thickness, nr=FusedSilica.nr
        )
        self.AS_to_Tel_port = AS2AR.p1

        AS3 = model.add(
            fc.Beamsplitter(
                "AS3",
                T=ASpars.AS3.HR.T,
                L=ASpars.AS3.HR.L,
                Rc=ASpars.AS3.HR.Rc,
                alpha=ASpars.AS3.HR.AOI,
            )
        )

        model.connect(AS2.p2, AS3.p1, L=ASpars.length_AS2HR_AS3HR)

        OFI = model.add(fc.DirectionalBeamsplitter("OFI"))

        model.connect(
            AS3.p2,
            OFI.p1,
            L=float(ASpars.length_AS3HR_AS4HR) - float(ASpars.length_OFI_AS4),
        )

        AS4 = model.add(
            fc.Beamsplitter(
                "AS4",
                T=ASpars.AS4.HR.T,
                L=ASpars.AS4.HR.L,
                Rc=ASpars.AS4.HR.Rc,
                alpha=ASpars.AS4.HR.AOI,
            )
        )

        model.connect(OFI.p3, AS4.p1, L=ASpars.length_OFI_AS4)

        self.AS_to_BH_port = AS4.p2

        self.add_AS_telescopes(model, ASpars)

    def add_LO_path(self, model, BHparams):
        try:
            assert hasattr(self, "PR2_for_port")
        except AssertionError:
            raise NotImplementedError("LO requires options.PR2_PR3_substrates = True")

        LO1 = model.add(
            fc.Beamsplitter(
                "LO1",
                T=BHparams.LO1.HR.T,
                L=BHparams.LO1.HR.L,
                Rc=BHparams.LO1.HR.Rc,
                alpha=BHparams.LO1.HR.AOI,
            )
        )

        model.connect(self.PR2_for_port, LO1.p1, L=BHparams.length_PR2AR_LO1HR)

        LO2 = model.add(
            fc.Beamsplitter(
                "LO2",
                T=BHparams.LO2.HR.T,
                L=BHparams.LO2.HR.L,
                Rc=BHparams.LO2.HR.Rc,
                alpha=BHparams.LO2.HR.AOI,
            )
        )

        model.connect(LO1.p2, LO2.p1, L=BHparams.length_LO1HR_LO2HR)

        LO3 = model.add(
            fc.Beamsplitter(
                "LO3",
                T=BHparams.LO3.HR.T,
                L=BHparams.LO3.HR.L,
                Rc=BHparams.LO3.HR.Rc,
                alpha=BHparams.LO3.HR.AOI,
            )
        )

        model.connect(LO2.p2, LO3.p1, L=BHparams.length_LO2HR_LO3HR)

        LO4 = model.add(
            fc.Beamsplitter(
                "LO4",
                T=BHparams.LO4.HR.T,
                L=BHparams.LO4.HR.L,
                Rc=BHparams.LO4.HR.Rc,
                alpha=BHparams.LO4.HR.AOI,
            )
        )

        model.connect(LO3.p2, LO4.p1, L=BHparams.length_LO3HR_LO4HR)

        BHDBS = model.add(
            fc.Beamsplitter(
                "BHDBS",
                T=BHparams.BHDBS.T,
                L=BHparams.BHDBS.L,
                alpha=BHparams.BHDBS.AOI,
            )
        )

        model.connect(self.AS_to_BH_port, BHDBS.p1, L=BHparams.length_AS4HR_BHDBS)

        # Need to propagate to a window so connected PD's face space
        BHDBS_OUT1_Window = model.add(fc.Nothing("BHDBS_OUT1_Window"))
        model.connect(BHDBS.p2, BHDBS_OUT1_Window.p1, L=BHparams.length_BHDBS1_Window)
        self.BHDBS_OUT1 = BHDBS_OUT1_Window.p1

        alpha_AR, L_BS_sub = self._BS_substrate_length(BHDBS, BHparams.BHDBS)

        BHDBS_OUT2 = model.add(
            fc.Beamsplitter(
                "BHDBS_OUT2",
                R=BHparams.BHDBS.R_AR,
                L=BHparams.BHDBS.L_AR,
                alpha=np.rad2deg(alpha_AR),
            )
        )

        model.connect(BHDBS.p3, BHDBS_OUT2.p3, L=L_BS_sub)

        # Need to propagate to a window so connected PD's face space
        BHDBS_OUT2_Window = model.add(fc.Nothing("BHDBS_OUT2_Window"))
        model.connect(
            BHDBS_OUT2.p1, BHDBS_OUT2_Window.p1, L=BHparams.length_BHDBS1_Window
        )
        self.BHDBS_OUT2 = BHDBS_OUT2_Window.p1

        BHDBS_LO = model.add(
            fc.Beamsplitter(
                "BHDBS_LO",
                R=BHparams.BHDBS.R_AR,
                L=BHparams.BHDBS.L_AR,
                alpha=np.rad2deg(alpha_AR),
            )
        )

        model.connect(BHDBS.p4, BHDBS_LO.p3, L=L_BS_sub)
        model.connect(LO4.p2, BHDBS_LO.p1, L=BHparams.length_LO4HR_BHDBS)

    def add_AS_telescopes(self, model, ASpars):
        ASL = model.add(fc.Lens("ASL", ASpars.ASL.f))
        model.connect(self.AS_to_Tel_port, ASL.p1, L=ASpars.length_AS2AR_ASL)

        window1 = model.add(
            fc.Beamsplitter(
                "AS_Window_Vac",
                R=ASpars.window.R,
                L=ASpars.window.L,
                alpha=ASpars.window.alpha,
            )
        )
        model.connect(ASL.p2, window1.p1, L=ASpars.length_ASL_window)

        window2 = model.add(
            fc.Beamsplitter(
                "AS_Window_Air",
                R=ASpars.window.R,
                L=ASpars.window.L,
                alpha=ASpars.window.alpha,
            )
        )

        model.connect(
            window1.p3,
            window2.p3,
            name="subAS_Window",
            L=ASpars.window.thickness,
            nr=FusedSilica.nr,
        )

        self._link_all_mechanical_dofs(window1, window2, facing=True)

        self.as_window_out = window2.p1
        self.add_AS_table(model)

    def _sus_poles(self, Q, res_freq):
        """res_freq is in Hz!"""
        return (
            2
            * np.pi
            * np.array(
                [
                    -res_freq / (2 * Q) * (1 + 1j * np.sqrt(4 * Q**2 - 1)),
                    -res_freq / (2 * Q) * (1 - 1j * np.sqrt(4 * Q**2 - 1)),
                ]
            )
        )

    def SOS_ZPK_dof(self, optic, param, direction):
        """Generate a Zero-Pole-Gain (ZPK) tuple for a suspension degree of freedom
        (DOF) for a specific optic.

        This method computes the ZPK model for a suspension system in a specific degree of freedom by reading
        configuration parameters and determining necessary values such as quality factor, resonance frequency,
        and mass or moment of inertia. Warnings are issued if required parameters are missing, and reasonable
        defaults are assumed in such cases.

        Parameters
        ----------
        optic : object
            The optical element for which the suspension ZPK is computed. This object is expected to have a `name`
            attribute for identification in warning messages.
        param : dict
            Dictionary containing the suspension configuration parameters. Expected keys include values like
            'sus_Q_{direction}', 'sus_f0_{direction}', 'mass', 'MOI_{pitch, yaw}', 'thickness', and 'radius'.
        direction : str
            The suspension degree of freedom for which to compute the ZPK. Accepted values are 'pos' (position),
            'pitch', and 'yaw', each specifying a different DOF of the optic.

        Returns
        -------
        tuple of lists
            A tuple containing:

            - z : list
                List of zeros for the ZPK model (empty in this case).
            - p : list
                List of poles derived from the suspension system parameters based on the DOF.
            - k : float
                Gain factor computed from the mass or moment of inertia, depending on the DOF.

        Notes
        -----
        The function assumes default values for missing parameters and logs a warning.
        For instance, if 'MOI_pitch' or 'MOI_yaw' are missing, the moment of inertia is estimated
        using standard formulas based on thickness, radius, and mass.

        Examples
        --------
        >>> from finesse.components import Mirror
        >>> from finesse_40m.factory import FortyMeterFactory
        >>> M = Mirror('name', R=1, L=0)
        >>> param = {'sus_Q_pos': 5, 'sus_f0_pos': 0.99, 'mass': 0.2642}
        >>> factory = FortyMeterFactory()
        >>> z, p, k = factory.SOS_ZPK_dof(M, param, 'pos')
        """

        def get_and_warn(key, default):
            var = param.get(key, None)
            if var is None:
                if key == "MOI_pitch" or key == "MOI_yaw":
                    thickness = float(get_and_warn("thickness", 25e-3))
                    radius = float(get_and_warn("radius", 75e-3 / 2))
                    mass = float(get_and_warn("mass", 0.2642))
                    var = (1 / 12) * mass * ((3 * (radius**2)) + (thickness**2))
                else:
                    logger.info(
                        f"{optic.name} does not have attribute {key} in config file. "
                        f"Assuming {default}."
                    )
                    var = default
            return var

        z = []
        if direction == "pos":
            p = self._sus_poles(
                get_and_warn("sus_Q_" + direction, 5),
                get_and_warn("sus_f0_" + direction, 0.99),
            )
            k = 1 / get_and_warn("mass", 0.2642)
        elif direction == "pitch":
            p = self._sus_poles(
                get_and_warn("sus_Q_" + direction, 5),
                get_and_warn("sus_f0_" + direction, 0.7),
            )
            k = 1 / get_and_warn("MOI_" + direction, np.inf)
        elif direction == "yaw":
            p = self._sus_poles(
                get_and_warn("sus_Q_" + direction, 5),
                get_and_warn("sus_f0_" + direction, 0.8),
            )
            k = 1 / get_and_warn("MOI_" + direction, np.inf)

        return z, p, k

    def SOS_plant_ZPK(self, model, optic, param):
        plant = {}
        plant["z", "F_z"] = self.SOS_ZPK_dof(optic, param, "pos")
        plant["pitch", "F_pitch"] = self.SOS_ZPK_dof(optic, param, "pitch")
        plant["yaw", "F_yaw"] = self.SOS_ZPK_dof(optic, param, "yaw")

        model.add(fc.mechanical.SuspensionZPK(optic.name + "_sus", optic.mech, plant))

    def add_suspensions(self, model, params):
        """This function adds the suspensions.

        Overwite this function to change the ZPK model for a particular optic
        """
        if self.options.suspensions.test_masses:
            self.SOS_plant_ZPK(model, model.ITMX, params.X.ITM)
            self.SOS_plant_ZPK(model, model.ITMY, params.Y.ITM)
            self.SOS_plant_ZPK(model, model.ETMX, params.X.ETM)
            self.SOS_plant_ZPK(model, model.ETMY, params.Y.ETM)

        if self.options.suspensions.core_optics:
            self.SOS_plant_ZPK(model, model.BS, params.BS)
            self.SOS_plant_ZPK(model, model.PRM, params.PRC.PRM)

        if self.options.suspensions.auxillary_optics:
            self.SOS_plant_ZPK(model, model.AS1, params.AS.AS1)
            self.SOS_plant_ZPK(model, model.AS4, params.AS.AS4)
            self.SOS_plant_ZPK(model, model.LO1, params.BH.LO1)
            self.SOS_plant_ZPK(model, model.LO2, params.BH.LO2)
            self.SOS_plant_ZPK(model, model.PR2, params.PRC.PR2)
            self.SOS_plant_ZPK(model, model.PR3, params.PRC.PR3)
            self.SOS_plant_ZPK(model, model.SR2, params.SRC.SR2)

    def add_AS_table(self, model):
        """Overwrite this method.

        This is included simply because finesse by default connects detectors to face
        the space (the input), and so we can't connect to AR sides of things easily
        """
        AS = model.add(fc.Nothing("AS_port"))

        model.connect(self.as_window_out, AS.p1, L=0.1)
        self.AS_port = AS.p1

    def add_POP_telescopes(self, model, POPpars):
        try:
            assert hasattr(self, "PR2_rev_port")
        except AssertionError:
            raise NotImplementedError(
                "POP telescopes require options.PR2_PR3_substrates = True"
            )

        POP_SM4 = model.add(
            fc.Beamsplitter(
                "POP_SM4",
                T=POPpars.POP_SM4.HR.T,
                L=POPpars.POP_SM4.HR.L,
                alpha=POPpars.POP_SM4.HR.AOI,
            )
        )
        model.connect(self.PR2_rev_port, POP_SM4.p1, L=POPpars.length_PR2AR_SM4)

        POP_SM5 = model.add(
            fc.Beamsplitter(
                "POP_SM5",
                T=POPpars.POP_SM5.HR.T,
                L=POPpars.POP_SM5.HR.L,
                alpha=POPpars.POP_SM5.HR.AOI,
            )
        )
        model.connect(POP_SM4.p2, POP_SM5.p1, L=POPpars.length_SM4_window)

        window1 = model.add(
            fc.Beamsplitter(
                "POP_Window_Vac",
                R=POPpars.window.AR.R,
                L=POPpars.window.AR.L,
                alpha=POPpars.POP_SM5.HR.AOI,
            )
        )
        model.connect(POP_SM5.p2, window1.p1, L=POPpars.length_SM4_SM5)

        window2 = model.add(
            fc.Beamsplitter(
                "POP_Window_Air",
                R=POPpars.window.AR.R,
                L=POPpars.window.AR.L,
                alpha=POPpars.POP_SM5.HR.AOI,
            )
        )

        model.connect(
            window1.p3,
            window2.p3,
            name="subSVC_SW_Window_1",
            L=POPpars.window.thickness,
            nr=FusedSilica.nr,
        )

        self._link_all_mechanical_dofs(window1, window2, facing=True)

        self.pop_window_out = window2.p1
        self.add_pop_table(model)

    def add_pop_table(self, model):
        """Overwrite this method.

        This is included simply because finesse by default connects detectors to face
        the space, and so we can't connect to AR sides of things easily
        """
        POP = model.add(fc.Nothing("POP_port"))

        model.connect(self.pop_window_out, POP.p1, L=0.1)
        self.POP_port = POP.p1  # POP PDs are added to .i

    def add_input_path(self, model, INPUT, PRM_AR_port):
        """Setup the input path, starting from the laser and going to the PRM."""
        LASER = model.add(fc.Laser("L0", P=INPUT.LASER.power))

        f1 = model.add_parameter("f1", INPUT.LASER.f1).ref
        f2 = model.add_parameter("f2", 5 * f1).ref
        f3 = model.add_parameter("f3", INPUT.LASER.f3).ref

        modulators = self.add_modulators(model, INPUT, f1, f2, f3)

        if len(modulators) == 0:
            LASER_port_out = LASER.p1
        else:
            model.link(LASER, *modulators)
            LASER_port_out = modulators[-1].p2

        IFI = model.add(fc.DirectionalBeamsplitter("IFI"))

        if self.options.INPUT.add_IMC_and_IM1:
            raise NotImplementedError
        else:
            model.link(LASER_port_out, IFI.p1)
            model.link(IFI.p3, INPUT.IMC.length_MMT2_TT2, PRM_AR_port)

            REFL_port = model.add(fc.Nothing("REFL_port"))
            model.connect(model.IFI.p4, REFL_port.p1)
            self.REFL_port = REFL_port.p1

            if self.options.INPUT.set_IMC_mode:
                model.add(
                    fc.Gauss(
                        "IMCmode",
                        LASER_port_out.o,
                        w0x=INPUT.IMC.qx.w0,
                        zx=INPUT.IMC.qx.z,
                        w0y=INPUT.IMC.qy.w0,
                        zy=INPUT.IMC.qy.z,
                    )
                )

    def add_modulators(self, model, INPUT, f1, f2, f3):
        """Add the modulators to the model.

        This function is called by
        :meth:`finesse_40m.factory.base.FortyMeterFactory.add_input_path`
        and should return a list of modulators. It takes INPUT as a
        parameter which should contain the INPUT section of the
        parameters file.

        It does not link these modulators to the model
        """
        modulators = []
        if self.options.add_11MHz:
            modulators.append(
                model.add(
                    fc.Modulator(
                        "mod1",
                        midx=INPUT.LASER.mod_depth_11,
                        f=f1,
                        mod_type="pm",
                        order=float(self.options.modulation_order),
                    )
                )
            )

        if self.options.add_55MHz:
            modulators.append(
                model.add(
                    fc.Modulator(
                        "mod2",
                        midx=INPUT.LASER.mod_depth_55,
                        f=f2,
                        mod_type="pm",
                        order=float(self.options.modulation_order),
                    )
                )
            )

        if self.options.add_165MHz:
            modulators.append(
                model.add(
                    fc.Modulator(
                        "mod3",
                        midx=INPUT.LASER.mod_depth_165,
                        f=f3,
                        mod_type="pm",
                        order=float(self.options.modulation_order),
                    )
                )
            )
        return modulators

    def add_cavities(self, model):
        model.add(fc.Cavity("cavXARM", model.ETMX.p1.o, priority=100))
        model.add(fc.Cavity("cavYARM", model.ETMY.p1.o, priority=100))
        model.add(fc.Cavity("cavPRX", self.PRM_HR_fr.o, via=self.ITMX_HR_bk.i))
        model.add(fc.Cavity("cavPRY", self.PRM_HR_fr.o, via=self.ITMY_HR_bk.i))
        model.add(
            fc.Cavity(
                "cavSRX",
                self.SRM_HR_fr.o,
                via=self.ITMX_HR_bk.i,
                priority=-50,
            )
        )
        model.add(
            fc.Cavity(
                "cavSRY",
                self.SRM_HR_fr.o,
                via=self.ITMY_HR_bk.i,
                priority=-50,
            )
        )

    def set_default_drives(self, L_force=True, P_torque=True, Y_torque=True):
        """Resets factory to all default drives.

        Parameters
        ----------
        L_force : bool
            Whether the length drive should be a force (True) or a
            displacement (False). Default: False.
        P_torque : bool
            Whether the pitch drive should be a torque (True) or an
            angle (False). Default: True.
        Y_torque : bool
            Whether the yaw drive should be a torque (True) or an
            angle (False). Default: True.
        """
        Ltype = "F_" if L_force else ""
        Ptype = "F_" if P_torque else ""
        Ytype = "F_" if Y_torque else ""
        self.local_drives = Munch(
            {
                "L": Munch(
                    {
                        "ITMX": {f"ITMX.mech.{Ltype}z": 1},
                        "ETMX": {f"ETMX.mech.{Ltype}z": 1},
                        "ITMY": {f"ITMY.mech.{Ltype}z": 1},
                        "ETMY": {f"ETMY.mech.{Ltype}z": 1},
                        "PRM": {f"PRM.mech.{Ltype}z": 1},
                        "SRM": {f"SRM.mech.{Ltype}z": 1},
                        "BS": {f"BS.mech.{Ltype}z": 1},
                    }
                ),
                "P": Munch(
                    {
                        "ITMX": {f"ITMX.mech.{Ptype}pitch": 1},
                        "ETMX": {f"ETMX.mech.{Ptype}pitch": 1},
                        "ITMY": {f"ITMY.mech.{Ptype}pitch": 1},
                        "ETMY": {f"ETMY.mech.{Ptype}pitch": 1},
                        "PRM": {f"PRM.mech.{Ptype}pitch": 1},
                        "PR2": {f"PR2.mech.{Ptype}pitch": 1},
                        "SRM": {f"SRM.mech.{Ptype}pitch": 1},
                        "SR2": {f"SR2.mech.{Ptype}pitch": 1},
                        "BS": {f"BS.mech.{Ptype}pitch": 1},
                        "IM4": {f"IM4.mech.{Ptype}pitch": 1},
                    }
                ),
                "Y": Munch(
                    {
                        "ITMX": {f"ITMX.mech.{Ytype}yaw": 1},
                        "ETMX": {f"ETMX.mech.{Ytype}yaw": 1},
                        "ITMY": {f"ITMY.mech.{Ytype}yaw": 1},
                        "ETMY": {f"ETMY.mech.{Ytype}yaw": 1},
                        "PRM": {f"PRM.mech.{Ytype}yaw": 1},
                        "PR2": {f"PR2.mech.{Ytype}yaw": 1},
                        "SRM": {f"SRM.mech.{Ytype}yaw": 1},
                        "SR2": {f"SR2.mech.{Ytype}yaw": 1},
                        "BS": {f"BS.mech.{Ytype}yaw": 1},
                        "IM4": {f"IM4.mech.{Ytype}yaw": 1},
                    }
                ),
            }
        )

    def set_default_LSC(self, LSC_params=None):
        """Resets factory to all default LSC settings.

        Parameters
        ----------
        LSC_params : MunchDict | str | None
            Munch dict of parameters, or a filename and path. If set to None, then
            self.params.CONTROL will be used.
        """
        if LSC_params is None:
            LSC_params = self.params.CONTROL

        self.LSC_output_matrix = Munch(
            {
                "XARM": Munch(),
                "YARM": Munch(),
                "CARM": Munch(),
                "DARM": Munch(),
                "PRCL": Munch(),
                "SRCL": Munch(),
                "MICH": Munch(),
                "MICH2": Munch(),
            }
        )

        # Note that if lx, ly, Lp, and Ls are the distances between the BS and ITMX, the
        # BS and ITMY, the PRC, and the SRC, respectively, then the MICH defined in this
        # output matrix is really driving lm = lx - ly = MICH as well as
        # Lp = -MICH and Ls = +MICH and that the MICH2 defined in this output matrix is
        # driving lm = MICH2 and no other degrees of freedom
        self.LSC_output_matrix.XARM.ETMX = +1
        self.LSC_output_matrix.YARM.ETMY = +1
        self.LSC_output_matrix.CARM.ETMX = (
            +0.5
        )  # Not physical, this actuates to laser freq
        self.LSC_output_matrix.CARM.ETMY = (
            +0.5
        )  # Not physical, this actuates to laser freq
        self.LSC_output_matrix.DARM.ETMX = +1  #
        self.LSC_output_matrix.DARM.ETMY = -1
        self.LSC_output_matrix.PRCL.PRM = +1
        self.LSC_output_matrix.SRCL.SRM = +1
        self.LSC_output_matrix.MICH.BS = +np.sqrt(
            2
        )  # This is (2) not root 2 in the CDS
        # model, but needs to be root(2) here
        # as we are not moddelling coil strenth
        self.LSC_output_matrix.MICH2.BS = +np.sqrt(
            2
        )  # This is (2) not root 2 in the CDS
        # model, but needs to be root(2) here
        # as we are not moddelling coil strenth

        self.LSC_output_matrix.MICH2.PRM = -1
        self.LSC_output_matrix.MICH.PRM = -1

        # This is theorertical since we dont have a SRM
        # self.LSC_output_matrix.MICH2.SRM = -1
        # self.LSC_output_matrix.MICH.SRM = -1

        self.LSC_output_matrix.MICH2.PRM = -1
        self.LSC_output_matrix.MICH2.SRM = +1
        ######
        # Output matrix is C1LSC_OUTPUT_MTRX.adl
        # sitemap -> LSC -> C1LSC -> OUT MTRX
        # ######
        # In general this varies depending on state, but for now we
        # will approximate this as being the same
        #######

        self.LSC_input_matrix = Munch(
            {
                "XARM": Munch(),
                "YARM": Munch(),
                "CARM": Munch(),
                "DARM": Munch(),
                "PRCL": Munch(),
                "SRCL": Munch(),
                "MICH": Munch(),
                "MICH2": Munch(),
            }
        )
        ######
        # Input matrix is C1LSC_INPUT_MTRX.adl
        # sitemap -> LSC -> C1LSC -> IN MTRX
        # ######
        self.LSC_input_matrix.DARM["AS55.Q"] = 1
        self.LSC_input_matrix.CARM["REFL9.I"] = 1
        self.LSC_input_matrix.MICH2["POP45.Q"] = 1
        self.LSC_input_matrix.SRCL["POP45.I"] = 1
        self.LSC_input_matrix.PRCL["POP9.I"] = 1

        self.LSC_controller = Munch(
            PRCL=(
                "PRCL_controller",
                np.array(LSC_params.PRCL["zeros"]),
                np.array(LSC_params.PRCL["poles"]),
                -LSC_params.PRCL["gain"],
            )
        )

    def add_LSC_readouts(self, model):
        f1 = model.f1.ref
        f2 = model.f2.ref
        output_detectors = self.options.LSC.add_output_detectors
        model.add(
            fc.ReadoutRF(
                "REFL11", self.REFL_port.i, f=f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "REFL55", self.REFL_port.i, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP11", self.POP_port.i, f=f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP22", self.POP_port.i, f=2 * f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP44", self.POP_port.i, f=f2 - f1, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP55", self.POP_port.i, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "POP110", self.POP_port.i, f=2 * f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "AS55", self.AS_port.i, f=f2, output_detectors=output_detectors
            )
        )
        model.add(
            fc.ReadoutRF(
                "AS44", self.AS_port.i, f=f2 - f1, output_detectors=output_detectors
            )
        )

        rout = self.options.OUTPUT.readout
        if rout == "AS":
            port = self.AS_port.i
        elif rout == "HD":
            port = self.OMC_output_port.o
        elif rout == "BHD":
            raise NotImplementedError("BHD not implemented!")
        else:
            NotImplementedError("Readout of {rout} not implemented for AS readout!")

        model.add(fc.ReadoutDC("AS", port, output_detectors=output_detectors))

    def add_detectors(self, model):
        if self.add_ADs():
            f1 = model.f1.ref
            f2 = model.f2.ref

            def add_amplitude_detectors(port, key):
                freqs = []
                if self.options.add_11MHz:
                    freqs.append((f1, "11"))
                if self.options.add_55MHz:
                    freqs.append((f2, "55"))
                self._add_all_amplitude_detectors(model, port, key, *freqs)

        readout_method = self.options.OUTPUT.readout

        if self.add_PDs:
            model.add(PowerDetector("Pin", self.PRM_AR_fr.i))
            Px = Constant(model.Px)
            Py = Constant(model.Py)

            model.add(PowerDetector("PreflPRM", self.PRM_AR_fr.o))
            model.add(PowerDetector("Prefl", self.REFL_port.i))
            model.add(PowerDetector("Ppop", self.POP_port.i))
            model.add(PowerDetector("Pas", self.AS_port.i))
            if readout_method in ["HD"]:
                model.add(PowerDetector("Pas_c", model.OM1.p1.i))
                model.add(PowerDetector("Pas", self.OMC_output_port.o))
            Pinx = Constant(model.Pinx)
            Piny = Constant(model.Piny)

        if self.add_ADs():
            add_amplitude_detectors(self.POP_port.i, "pop")
            if readout_method in ["HD"]:
                add_amplitude_detectors(self.OMC_output_port.o, "dcpd")
            add_amplitude_detectors(self.AS_port.i, "as")
            add_amplitude_detectors(model.PRM.p2.i, "in")
            add_amplitude_detectors(self.REFL_port.i, "refl")
            add_amplitude_detectors(model.PRM.p1.o, "prc")
            freqs = [
                (f1, "11"),
                (f2, "55"),
                (f2, "55", 0, 0),
            ]
            self._add_all_amplitude_detectors(model, model.SRM.p1.i, "src", *freqs)

            model.add(AmplitudeDetector("a_carrier_refl", self.REFL_port.i, f=0))
            a_carrier_pinx = Constant(model.a_carrier_pinx)
            a_carrier_piny = Constant(model.a_carrier_piny)
            a_carrier_x = Constant(model.a_carrier_x)
            a_carrier_y = Constant(model.a_carrier_y)

            # FIXME: there should be some logic below for if 9 and 45 MHz have been added
            Pprc_carrier = abs(Constant(model.a_c0_prc)) ** 2
            Pin_carrier = abs(Constant(model.a_c0_in)) ** 2
            Pprc_11 = (
                abs(Constant(model.a_u11_prc)) ** 2
                + abs(Constant(model.a_l11_prc)) ** 2
            )
            Pprc_55 = (
                abs(Constant(model.a_u55_prc)) ** 2
                + abs(Constant(model.a_l55_prc)) ** 2
            )
            Pin_11 = (
                abs(Constant(model.a_u11_in)) ** 2 + abs(Constant(model.a_l11_in)) ** 2
            )
            Pin_55 = (
                abs(Constant(model.a_u55_in)) ** 2 + abs(Constant(model.a_l55_in)) ** 2
            )
            Pinx_carrier = abs(a_carrier_pinx) ** 2
            Piny_carrier = abs(a_carrier_piny) ** 2

            model.add(MathDetector("Pprc_carrier", Pprc_carrier))
            model.add(MathDetector("Pprc_9", Pprc_11))
            model.add(MathDetector("Pprc_45", Pprc_55))
            model.add(MathDetector("Pin_carrier", Pin_carrier))
            model.add(MathDetector("Pin_9", Pin_11))
            model.add(MathDetector("Pin_45", Pin_55))
            model.add(MathDetector("Pinx_carrier", Pinx_carrier))
            model.add(MathDetector("Piny_carrier", Piny_carrier))
            model.add(MathDetector("Pas_carrier", abs(Constant(model.a_c0_src)) ** 2))

            model.add(MathDetector("PRG", Pprc_carrier / Pin_carrier))
            model.add(MathDetector("PRG11", Pprc_11 / Pin_11))
            model.add(MathDetector("PRG55", Pprc_55 / Pin_55))
            model.add(MathDetector("AGX", abs(a_carrier_x) ** 2 / Pinx_carrier))
            model.add(MathDetector("AGY", abs(a_carrier_y) ** 2 / Piny_carrier))
            model.add(MathDetector("cost_prcl", Pprc_11 * (Px + Py) / 2))
            # minimize the PRG for the 45 and the carrier through the the OMC for RSE
            model.add(
                MathDetector(
                    "cost_srcl",
                    Constant(model.PRG55)
                    * 1
                    / Constant(model.a_u55_00_src)
                    * 1
                    / Constant(model.a_l55_00_src),
                )
            )

    def add_LSC_DC_locks(self, model):
        """Do not use.

        This function exists in `finesse_ligo`, however, it is not implemented
        in `finesse_40m` as we have too many locking confiurations.

        See :mod:`finesse_40m.locks` and add the locks you want.
        """
        raise NotImplementedError

    def add_LSC_AC_loops(self, model):
        """Do not use.

        This function exists in `finesse_ligo`, however, it is not implemented
        in `finesse_40m` as we have too many locking confiurations.

        See :mod:`finesse_40m.locks` and add the locks you want.
        """
        raise NotImplementedError
