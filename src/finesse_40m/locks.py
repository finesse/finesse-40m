from .logging import get_logger

# Set up logger for module_a
logger = get_logger()

import finesse
import finesse.analysis.actions as fac
import pprint

pp = pprint.PrettyPrinter(indent=4, sort_dicts=True).pprint


def add_PRMI_locks(model):
    """Add PRMI (Power-Recycled Michelson Interferometer) locks to the Finesse model.

    This function adds two locks:
    1. `MICH_lock` - Locks the Michelson interferometer to maintain the
       correct differential arm length.
    2. `PRCL_lock` - Locks the power-recycling cavity to the correct length.

    Both locks are configured to maintain the DC operating point.

    Parameters
    ----------
    model : finesse.Model
        The Finesse model to which the locks are added.

    Returns
    -------
    None
    """
    model.add(
        finesse.locks.Lock("MICH_lock", model.AS55.outputs.I, model.MICH.DC, -15, 1e-6)
        # See /opt/rtcds/caltech/c1/Git/40m/scripts/LSC/PRMI-AS55_REFL55.yaml
        # I, Q and somewhat arbitrary in "reality", but in finesse we must use I
        # HEREBE DRAGONS: I'm not 100% sure on the above statement. I think one
        # must actually implement the "phasing" that is implemented in the aLIGO models
    )
    model.add(
        finesse.locks.Lock(
            "PRCL_lock", model.REFL11.outputs.I, model.PRCL.DC, 2.8, 1e-6
        )
    )


def add_PRMI_LSC_AC_feedback(model, factory):
    """Add AC feedback loops for PRMI in the LSC (Length Sensing and Control) system.

    This function modifies the input and output matrices in the LSC system
    to include the feedback behavior of the AC loops. It ensures that only
    the necessary degrees of freedom (DOFs) are active, and it disables unused ones.

    Parameters
    ----------
    model : finesse.Model
        The Finesse model to which the AC feedback loops are added.
    factory : FortyMeterFactory
        The factory instance used to configure the LSC system.

    Returns
    -------
    None
    """

    factory.LSC_input_matrix.MICH["AS55.I"] = 1
    factory.LSC_input_matrix.PRCL.pop("POP9.I", "not present")
    factory.LSC_input_matrix.PRCL["REFL11.I"] = 1
    removed_LSC = ["DARM", "CARM", "MICH2", "SRCL", "XARM", "YARM"]

    for k in removed_LSC:
        v = factory.LSC_input_matrix.pop(k, "not present")
        logger.info(f"Removing {k} from input matrix. Old value was {v}")
        v = factory.LSC_output_matrix.pop(k, "not present")
        logger.info(f"Removing {k} from output matrix. Old value was {v}")

    factory._add_AC_loops(
        model,
        factory.local_drives["L"],
        factory.LSC_input_matrix,
        factory.LSC_output_matrix,
        factory.LSC_controller,
        factory.options.LSC,
    )


def print_AC_loop_info(factory):
    """Print diagnostic information about the AC loops in the LSC system.

    This function outputs information about:
    - Local drives
    - Input matrix
    - Output matrix
    - LSC controller

    Parameters
    ----------
    factory : FortyMeterFactory
        The factory instance containing the LSC system configuration.

    Returns
    -------
    None
    """
    print("===== Local Drives =======")
    pp(factory.local_drives["L"].toDict())

    print("\n\n===== Input Matrix =======")
    pp(factory.LSC_input_matrix.toDict())

    print("\n\n===== Output Matrix =======")
    pp(factory.LSC_output_matrix.toDict())

    print("\n\n===== LSC Controller =======")
    pp(factory.LSC_controller)


def plot_PRMI_error_signals(model):
    """Plot error signals for PRMI locks.

    This function generates a plot of the error signals for the active PRMI locks,
    normalized and displayed as a function of an offset parameter. It helps
    visualize how the error signals behave around the lock point.

    Parameters
    ----------
    model : finesse.Model
        The Finesse model for which the error signals are plotted.

    Returns
    -------
    None
    """
    import matplotlib.pyplot as plt

    error_signal = fac.Series(
        # fac.Xaxis(model.CARM_lock.feedback, 'lin', -0.1, 0.1, 100, relative=True, name=model.CARM_lock.name),
        # fac.Xaxis(model.DARM_rf_lock.feedback, 'lin', -0.1, 0.1, 100, relative=True, name=model.DARM_rf_lock.name),
        fac.Xaxis(
            model.PRCL_lock.feedback,
            "lin",
            -10,
            10,
            100,
            relative=True,
            name=model.PRCL_lock.name,
        ),
        # fac.Xaxis(model.SRCL_lock.feedback, 'lin', -20, 20, 100, relative=True, name=model.SRCL_lock.name),
        fac.Xaxis(
            model.MICH_lock.feedback,
            "lin",
            -5,
            5,
            100,
            relative=True,
            name=model.MICH_lock.name,
        ),
    )

    def getls(lss=["-", "--", ":"]):
        i = 0
        while True:
            yield lss[i]
            i += 1
            if i >= len(lss):
                i = 0

    def plot_error_signals(model, errsig_sols):
        plt.figure()
        lss = getls()
        for lock in model.locks:
            if lock.enabled:
                sol = errsig_sols[lock.name]
                x = sol.x[0] / sol.x[0].max()
                y = sol[lock.error_signal.name]
                if y.max() != 0:
                    y /= y.max()
                plt.plot(x, y, label=lock.error_signal.name, ls=next(lss))
        plt.legend()
        plt.xlabel("Offset [arb]")
        plt.ylabel("Error signal [arb]")

    errsig_sols = model.run(error_signal)
    plot_error_signals(model, errsig_sols)
