from .logging import get_logger

# Set up logger for module_a
logger = get_logger()

from finesse.analysis.actions import (
    Maximize,
    Minimize,
    Series,
    Change,
    Noxaxis,
    Execute,
    OptimiseRFReadoutPhaseDC,
    SetLockGains,
    RunLocks,
)

import finesse_ligo
from finesse_ligo.actions import DRFPMI_state, InitialLockLIGO

LSC_demod_opt_default = (
    "CARM",
    "REFL11_I",
    "PRCL",
    "POP11_I",
    "SRCL",
    "POP55_I",
    "DARM",
    "AS55_Q",
)


def InitialLock40(LSC_demod_opt=LSC_demod_opt_default, **kwargs):
    return InitialLockLIGO(LSC_demod_opt=LSC_demod_opt, **kwargs)


def InitialLockPRMI(
    LSC_demod_opt=("MICH", "AS55_I", "PRCL", "REFL11_I"),
    run_locks=True,
    exception_on_lock_fail=True,
    exception_on_check_fail=True,
    gain_scale=0.5,
    lock_steps=2000,
):
    def ALERT(x):
        if exception_on_check_fail:
            raise finesse_ligo.exceptions.InitialLockCheckException(x)
        else:
            logger.warn(x)

    def check_arm_gains(state, name):
        PRGX = state.previous_solution["Pinx"]
        PRGY = state.previous_solution["Piny"]

        if not (14 <= PRGX <= 16):
            ALERT(
                f"Power in PRX is significantly different to what was expected = {PRGX:.1f} W vs {15:.1f} W"
            )
        if not (14 <= PRGY <= 16):
            ALERT(
                f"Power in PRY is significantly different to what was expected = {PRGY:.1f} W vs {15:.1f} W"
            )

    action = Series(
        DRFPMI_state("PRMI"),
        Change(
            {
                "PRM.misaligned": True,
                "SRM.misaligned": True,
                "CARM.DC": 0,
                "DARM.DC": 0,
                "SRCL.DC": 0,
                "PRCL.DC": 0,
                "MICH.DC": 0,
                "MICH2.DC": 0,
            },
            name="misalign PRM,SRM",
        ),
        # Put mich on dark fringe
        Minimize("Pas_carrier", "MICH2.DC", name="find dark AS"),
        Noxaxis(name="after ARMs+AS"),
        # Realign the PRM
        Change({"PRM.misaligned": False}, name="align PRM"),
        # get the PRC in roughly the right place whilst keeping arms on resonance
        Maximize("PRG", "PRCL.DC", name="maximise PRG"),
        Noxaxis(name="after max PRG"),
        Execute(check_arm_gains),
        OptimiseRFReadoutPhaseDC(*LSC_demod_opt),
        SetLockGains(d_dof_gain=1e-10, gain_scale=gain_scale),
        RunLocks(max_iterations=lock_steps, exception_on_fail=exception_on_lock_fail),
        Noxaxis(name="after locks"),
        Execute(check_arm_gains),
    )
    return action
