import logging
import sys

logger_name = "finesse-40m"


def get_logger():
    return logging.getLogger(logger_name)


def setup_logger(context=None):
    """Sets up a logger for the given name. DEBUG messages are disabled, and all other
    messages (INFO, WARNING, ERROR, CRITICAL) are displayed on stderr.

    Args:
        name (str): Name of the logger.

    Returns:
        logging.Logger: Configured logger instance.
    """
    # Create a custom logger
    logger = get_logger()

    # Set the log level for the logger (DEBUG is disabled)
    logger.setLevel(logging.INFO)

    # Create a stream handler that outputs to stderr
    if context == "docs":
        console_handler = logging.StreamHandler(sys.stdout)
    else:
        console_handler = logging.StreamHandler()

    # Set the handler to log messages starting from INFO level
    console_handler.setLevel(logging.INFO)

    # Create a formatter and set it for the handler
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    console_handler.setFormatter(formatter)

    # Add the handler to the logger if it doesn't already exist
    if not logger.handlers:
        logger.addHandler(console_handler)

    return logger
