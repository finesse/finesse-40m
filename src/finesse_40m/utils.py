from .logging import get_logger

# Set up logger for module_a
logger = get_logger()

import finesse
from finesse.detectors import PowerDetector
from tqdm.auto import tqdm
import requests
from pathlib import Path
import shutil
import hashlib
import gzip


def get_readouts(model):
    """Retrieve RF readout names from the Finesse model.

    This function identifies all RF readouts in the model and returns their names,
    appending `_I` and `_Q` suffixes to represent the in-phase and quadrature components.

    Parameters
    ----------
    model : finesse.Model
        The Finesse model containing the readouts.

    Returns
    -------
    list of str
        A list of RF readout names with `_I` and `_Q` suffixes.
    """
    RFreadouts = []
    for rout in model.readouts:
        if isinstance(rout, finesse.components.readout.ReadoutRF):
            for quad in ["_I", "_Q"]:
                RFreadouts.append(rout.name + quad)
    return RFreadouts


def get_PDs(model):
    """Retrieve all power detectors from the Finesse model.

    This function filters the detectors in the model to include only instances
    of `PowerDetector`.

    Parameters
    ----------
    model : finesse.Model
        The Finesse model containing the detectors.

    Returns
    -------
    list of finesse.detectors.PowerDetector
        A list of all power detectors in the model.
    """
    return [
        detector for detector in model.detectors if isinstance(detector, PowerDetector)
    ]


def fmtpower(number):
    """Format a power value with the appropriate SI prefix.

    This function formats a power value (in Watts) using the appropriate unit
    and SI prefix for easier readability. For example, `1e7` becomes `10.0 MW`.

    Parameters
    ----------
    number : float
        The power value to format, in Watts.

    Returns
    -------
    str
        The formatted power value with the appropriate SI unit.
    """
    if number > 1e6:
        return f"{1e-6 * number:.1f} MW"
    elif number >= 1000:
        return f"{1e-3 * number:.1f} kW"
    elif number >= 1 or number == 0:
        return f"{number:.1f} W"
    elif number >= 1 / 1000:
        return f"{1e3 * number:.1f} mW"
    elif number >= 1e-6:
        return f"{1e6 * number:.1f} uW"
    else:
        return f"{number:.1e} W"


DATAFILES = {
    "2024_08_00-PRM_Force_Welch.npy": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/5dc44e57ddcf1718f4fda54e25223a4e/prm_force_welch.npy",
        "2aff54e55b93efb74306bd27bd50f32a",
    ),
    "2024_08_00-BS_Force_Welch.npy": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/15c8faaa040a12cefc2c7a09bdd16b47/bs_force_welch.npy",
        "ff07d88e3a317c0b9a7bb62ff2d21909",
    ),
    "PRCL_MICH_Act_Noise.h5": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/3a2132afa3fefa0146be10bedf7b5a58/PRCL_MICH_Act_Noise.h5",
        "a8afba3dd5febc1611d09623b49eb9ab",
    ),
    "C1LSC.txt": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/607cddfc68615de54b71599a0ad70853/C1LSC.txt",
        "80d09ea0e191202c7c4f0db0cda65656",
    ),
    "PRCL_MICH_sensing_noise.h5": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/ba136919fb1ae0461b5654d9230c92fc/PRCL_MICH_sensing_noise.h5",
        "95459d9f9ab090ab3a2cbb49cad6218b",
    ),
    "2024_10_15_1550_OLTF_PRCL_abs_rad.csv": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/534c8bc688f46f87435147fe24ad0bd2/2024_10_15_1550_OLTF_PRCL_abs_rad.csv",
        "0fefbd679502db5f671edd02106afac0",
    ),
    "2024_10_15_1550_OLTF_COHERE_PRCL_asis.csv": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/46c17ff904bcbd170639ea46bf9f0a39/2024_10_15_1550_OLTF_COHERE_PRCL_asis.csv",
        "9a06b33f8aac458cbcff40665bb16681",
    ),
    "2024_10_15_1550_OLTF_MICH.csv": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/e4696ca671d42a3df2192370b0fcbeb1/2024_10_15_1550_OLTF_MICH.csv",
        "9a2fc0e23af555915026c14d31b72984",
    ),
    "2024_10_15_1550_OLTF_COHERE_MICH.csv": (
        "https://git.ligo.org/finesse/finesse-40m/-/wikis/uploads/49cda0d79c2a3c854238b266de3b36c6/2024_10_15_1550_OLTF_COHERE_MICH.csv",
        "3f6e1b82e9c4e3e3477a6acc19e14cf3",
    ),
}


def is_gz_file(filepath):
    """Check if file is gzipped."""
    with open(filepath, "rb") as test_f:
        return test_f.read(2) == b"\x1f\x8b"


def download(datafile, DATAFILES=DATAFILES):
    """Download a data file from a specified URL and verify its checksum.

    This function downloads a data file defined in the `DATAFILES` dictionary.
    It verifies the file's checksum to ensure integrity. If the checksum does
    not match, it raises a `RuntimeError`.

    Parameters
    ----------
    datafile : str
        The name of the data file to download.
    DATAFILES : dict, optional
        A dictionary containing metadata about known data files. The keys are
        file names, and the values are tuples containing the URL and the expected
        MD5 checksum. The MD5 checksum can be None if it is not yet known.
        Defaults to the global `DATAFILES` variable.

    Returns
    -------
    None

    Raises
    ------
    FileNotFoundError
        If the `datafile` is not found in the `DATAFILES` dictionary.
    RuntimeError
        If the checksum verification fails.

    Examples
    --------
    Download a known data file with a custom DATAFILES dictionary:

    >>> custom_datafiles = {
    ...     "example_file.npy": (
    ...         "https://example.com/example_file.npy",
    ...         "d41d8cd98f00b204e9800998ecf8427e",  # Example checksum
    ...     )
    ... }
    >>> download("example_file.npy", DATAFILES=custom_datafiles)
    Writing data to /path/to/finesse-40m
    Downloading https://example.com/example_file.npy
    Successfully downloaded 'example_file.npy' with matching checksum.

    If the file is not listed in the `DATAFILES` dictionary:

    >>> download("nonexistent_file.npy", DATAFILES=custom_datafiles)
    Traceback (most recent call last):
        ...
    FileNotFoundError: Datafile nonexistent_file.npy is not known.
    """
    if datafile not in DATAFILES:
        raise FileNotFoundError(
            f"Datafile {datafile} is not known.\n"
            "Please specify a datafile dictionary with format {filename: (url, md5 hash)}.\n"
            "None can be used in place of the hash if it is not yet known."
        )

    # Get data installation path from finesse config
    cfg = finesse.config.config_instance()
    path = Path(cfg["finesse.data"]["path"]).expanduser().absolute() / "finesse-40m"
    path.mkdir(parents=True, exist_ok=True)
    print(f"Writing data to {path}")
    # make an HTTP request within a context manager
    with requests.get(DATAFILES[datafile][0], stream=True) as r:
        # check header to get content length, in bytes
        total_length = int(r.headers.get("Content-Length"))
        # implement progress bar via tqdm
        with tqdm.wrapattr(
            r.raw,
            "read",
            total=total_length,
            desc=f"Downloading {DATAFILES[datafile][0]}",
        ) as raw:
            # save the output to a file
            with open(path / datafile, "wb") as output:
                shutil.copyfileobj(raw, output)

    if is_gz_file(path / datafile):
        logger.info("File is gzipped. Unzipping")

        shutil.move(path / datafile, path / (str(datafile) + ".gz"))

        with gzip.open(path / (str(datafile) + ".gz"), "rb") as f_in:
            with open(path / datafile, "wb") as f_out:
                shutil.copyfileobj(f_in, f_out)

    else:
        logger.info("file is not gzipped")

    with open(path / datafile, "rb") as output:
        checksum = hashlib.md5(output.read()).hexdigest()

        if DATAFILES[datafile][1] is None:
            print(
                f"Checksum not previously known and cannot be checked. This checksum is: {checksum}"
            )

        elif checksum != DATAFILES[datafile][1]:
            raise RuntimeError(
                f"Checksum failed, downloaded file probably corrupted: {checksum} != {DATAFILES[datafile][1]}"
            )
        else:
            logger.info("checksum matches")


def get_file_path(file, DATAFILES=DATAFILES):
    """Retrieve the file path for a given data file, downloading it if necessary.

    This function checks if the specified data file exists locally. If it does
    not, it attempts to download it using the `download` function.

    Parameters
    ----------
    file : str
        The name of the data file to retrieve.
    DATAFILES : dict, optional
        A dictionary containing metadata about known data files. Defaults to the
        global `DATAFILES` variable. See `download` for more details about this
        parameter.

    Returns
    -------
    pathlib.Path
        The absolute path to the requested data file.

    Raises
    ------
    FileNotFoundError
        If the file is not found in the `DATAFILES` dictionary and cannot be downloaded.


    Examples
    --------
    Retrieve the path to a known data file, downloading it if necessary:

    >>> custom_datafiles = {
    ...     "example_file.npy": (
    ...         "https://example.com/example_file.npy",
    ...         "d41d8cd98f00b204e9800998ecf8427e",  # Example checksum
    ...     )
    ... }
    >>> path = get_file_path("example_file.npy", DATAFILES=custom_datafiles)
    Writing data to /path/to/finesse-40m
    Downloading https://example.com/example_file.npy
    Successfully downloaded 'example_file.npy' with matching checksum.
    >>> print(path)
    /path/to/finesse-40m/example_file.npy

    If the file already exists locally:

    >>> path = get_file_path("example_file.npy", DATAFILES=custom_datafiles)
    >>> print(path)
    /path/to/finesse-40m/example_file.npy

    If the file is not listed in the `DATAFILES` dictionary:

    >>> path = get_file_path("nonexistent_file.npy", DATAFILES=custom_datafiles)
    Traceback (most recent call last):
        ...
    FileNotFoundError: Could not find nonexistent_file.npy.

    If you do not yet know the MD5 checksum, then use None.

    >>> custom_datafiles = {
    ...     "example_file.npy": (
    ...         "https://example.com/example_file.npy",
    ...         None,  # Example checksum
    ...     )
    ... }
    >>> path = get_file_path("example_file.npy", DATAFILES=custom_datafiles)
    2024-11-27 17:38:27,380 - finesse-40m - WARNING - Could not find /Users/awjones/finesse-data/finesse-40m/example_file.npy.
    Will try downloading it using finesse_40m.tools.download('example_file.npy').
    Writing data to /path/to/finesse-40m
    Downloading https://example.com/example_file.npy
    Checksum not previously known and cannot be checked. This checksum is: 83f203952cf185ee46a3ed5
    """
    datapath = Path(finesse.config.config_instance()["finesse.data"]["path"])
    filepath = datapath / "finesse-40m" / file
    filepath = filepath.expanduser().absolute()
    if not filepath.exists():
        logger.warn(
            f"Could not find {filepath}.\n\nWill try downloading it using finesse_40m.tools.download('{str(file)}')."
        )

        download(file, DATAFILES=DATAFILES)
    return filepath
