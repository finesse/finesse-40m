"""Simulation Class for PRMI Environment.

This script defines the simulation. It uses a Finesse action as a post-step in a dummy scan to bypass beam tracing. This speeds the simulation up by several orders of magnitude.

Author: Kenny Moc
Email: kmoc@ualberta.ca
"""

import finesse.analysis.actions as fac
import numpy as np
from .pendulum import SinglePendulum
from .factory import pd_list, pdh_list
from finesse_40m.utils import get_file_path


class process_finesse_step(fac.random.Action):
    """A custom Finesse action that reads the current simulation workspaces (see Finesse
    docs for more details) and changes PRCL and MICH accordingly.

    Attributes:
        env (Pendulum_Environment): The PRMI environment.
        done (bool): Flag indicating if the simulation is complete.
    """

    def __init__(self, env, *args):
        super().__init__(self.__class__.__name__)
        self.args = tuple(a if isinstance(a, str) else a.full_name for a in args)
        self.env = env
        self.done = self.env.done

    def _requests(self, model, memo, first=True):
        """Records the parameters to be changed during the simulation."""

        memo["changing_parameters"].append("PRCL.DC")
        memo["changing_parameters"].append("MICH.DC")

    def _do(self, state):
        """Executes the action during each simulation step."""

        # Does not work at the moment
        # if self.done:
        #     quitter = pdb.Pdb()
        #     quitter.rcLines.append('quit')
        #     quitter.set_trace()

        # Workspace that holds current PD and PDH values
        self.observation = state.sim.workspace_name_map

        # The new prcl and mich values with the done flag
        self.prcl, self.mich, self.done = self.env.step(self.observation)

        state.model.PRCL.DC.value = self.prcl
        state.model.MICH.DC.value = self.mich


class Pendulum_Environment:
    """Environment simulating the PRMI pendulum system.

    Attributes:
        model: The Finesse model to be simulated.
        prm_pendulum (SinglePendulum): Pendulum representing the PRM.
        bs_pendulum (SinglePendulum): Pendulum representing the beam splitter.
        pd_list (list): List of photodetectors.
        pdh_list (list): List of photodetectors for homodyne detection.
    """

    def __init__(
        self, model, g, length, mass, dt, prm_force, bs_force, pd_list, pdh_list
    ):
        self.done = False
        self.model = model
        self.prm_pendulum = SinglePendulum(mass, length, g, dt, prm_force)
        self.bs_pendulum = SinglePendulum(mass, length, g, dt, bs_force)
        self.pd_list = pd_list
        self.pdh_list = pdh_list

        # Calculate MICH and PRCL displacements from the pendulum movements
        self.mich_change = (
            lambda bs_displacement, prm_displacement: np.sqrt(2) * bs_displacement
        )
        self.prcl_change = (
            lambda bs_displacement, prm_displacement: -bs_displacement / np.sqrt(2)
            + prm_displacement
        )

    def _observation_processing(self, observation):
        """Processes the workspace observation from Finesse into a dictionary.

        Parameters:
            observation: The workspaces from the Finesse step.

        Returns:
            dict: A dictionary mapping detector names to their outputs.
        """

        observation_dict = {}

        for signal in pd_list + pdh_list:
            observation_dict[signal] = observation[signal].get_output()

        return observation_dict

    def controller(self, observation_dict):
        """Placeholder controller function. Can be replaced with a more sophisticated
        control algorithm.

        Parameters:
            observation_dict (dict): Processed observation data.

        Returns:
            tuple: Applied PRM force, applied BS force, and done flag.
        """

        self.done = False
        return 0, 0, self.done

    def step(self, observation):
        """Performs a simulation step by applying forces to the pendulums and updating
        the Finesse model.

        Parameters:
            observation: The current observation from the Finesse simulation.

        Returns:
            tuple: Updated PRCL and MICH degrees, and done flag.
        """

        self.observation_dict = self._observation_processing(observation)

        self.applied_prm_force, self.applied_bs_force, self.done = self.controller(
            self.observation_dict
        )

        self.initial_prm_pos = self.prm_pendulum.x
        self.initial_bs_pos = self.bs_pendulum.x

        self.prm_pendulum.next_step(self.applied_prm_force)
        self.bs_pendulum.next_step(self.applied_bs_force)

        self.final_prm_pos = self.prm_pendulum.x
        self.final_bs_pos = self.bs_pendulum.x

        self.prm_change_in_pos = self.final_prm_pos - self.initial_prm_pos
        self.bs_change_in_pos = self.final_bs_pos - self.initial_bs_pos

        self.change_in_prcl_meters = self.prcl_change(
            self.bs_change_in_pos, self.prm_change_in_pos
        )
        self.change_in_mich_meters = self.mich_change(
            self.bs_change_in_pos, self.prm_change_in_pos
        )

        # Convert the PRCL and MICH displacement to degrees
        self.prcl_change_deg = self.change_in_prcl_meters * 1e9 / 1064 * 360
        self.mich_change_deg = self.change_in_mich_meters * 1e9 / 1064 * 360
        self.model.PRCL.DC += self.prcl_change_deg
        self.model.MICH.DC += self.mich_change_deg

        return self.model.PRCL.DC, self.model.MICH.DC, self.done


class Simulation:
    """Class to handle the entire simulation process, including the environment and
    Finesse model setup.

    Attributes:
        model: The Finesse model to be simulated.
        runtime (int): The duration of the simulation.
    """

    def __init__(self, model, runtime):
        # Sampling frequency and time step
        self.fs = 16384.0
        self.dt = 1 / self.fs
        self.g = 9.81
        self.runtime = runtime

        self.pendulum_length = (
            self.g / (2 * np.pi * 1) ** 2
        )  # Length of a 1 Hz pendulum
        self.pendulum_mass = 0.25  # kg

        # self.prm_force_dir = os.path.join(
        #     os.path.dirname(__file__), "prm_force_welch.npy"
        # )
        # self.bs_force_dir = os.path.join(
        #     os.path.dirname(__file__), "bs_force_welch.npy"
        # )
        self.prm_force_dir = get_file_path("2024_08_00-PRM_Force_Welch.npy")
        self.bs_force_dir = get_file_path("2024_08_00-BS_Force_Welch.npy")

        self.bs_force = np.load(self.prm_force_dir)
        self.prm_force = np.load(self.bs_force_dir)
        self.model = model

        # Set up the simulation environment
        self.env = Pendulum_Environment(
            model=model,
            g=self.g,
            length=self.pendulum_length,
            mass=self.pendulum_mass,
            dt=self.dt,
            prm_force=self.prm_force,
            bs_force=self.bs_force,
            pd_list=pd_list,
            pdh_list=pdh_list,
        )

        # The Finesse action to call between simulation steps
        self.step_processor = process_finesse_step(self.env)

        self.dummy_scan = fac.Xaxis(
            self.model.L0.phase, "lin", 0, 0, runtime, post_step=self.step_processor
        )

    @property
    def duration(self):
        return self.dt * self.runtime

    @property
    def t(self):
        return self.dt * np.arange(0, self.runtime, 1)

    def run(self):
        """Runs the Finesse simulation with the given setup.

        Returns:
            Results from the Finesse model run.
        """
        self.results = self.model.run(self.dummy_scan, progress_bar=True)
        return self.results
