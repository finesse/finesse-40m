"""Pendulum Model.

This script models a single pendulum in the presence of noise forces.
It generates a new noise force time series from the existing noise forces measured at the 40m (forces above 100Hz are disregarded).

Author: Kenny Moc
Email: kmoc@ualberta.ca
"""

import numpy as np


class SinglePendulum:
    """A class representing a single pendulum simulation.

    This class models the behavior of a single pendulum under the influence of gravity
    and an external force (noise). It uses the Runge-Kutta 4th order method for numerical integration.

    Attributes:
        mass (float): Mass of the pendulum bob in kg.
        length (float): Length of the pendulum in meters.
        g (float): Acceleration due to gravity in m/s^2.
        dt (float): Time step for simulation in seconds.
        num_steps (int): Total number of simulation steps.
        state (numpy.ndarray): Current state of the pendulum [theta, omega].
        position_history (list): History of pendulum bob positions.
        time_history (list): History of simulation time points.
        noise_force_series (numpy.ndarray): External force (noise) time series.
    """

    def _generate_new_noise(self, noise_spectrum, dt=1 / 16384):
        """Takes an existing noise time series and generates a new one by adding random
        phase."""

        # Fast fourier transform
        noise_fft = np.fft.rfft(noise_spectrum)
        noise_freqs = np.fft.rfftfreq(len(noise_spectrum), dt)

        # Adds random phases
        random_phases = np.random.uniform(-np.pi, np.pi, len(noise_freqs))
        new_spectrum = noise_fft * np.exp(1j * random_phases)

        # Inverse transform
        new_noise_series = np.fft.irfft(new_spectrum)

        return new_noise_series

    def __init__(self, mass, length, g, dt, noise_force):
        """Initialize the SinglePendulum object.

        Args:
            mass (float): Mass of the pendulum bob in kg.
            length (float): Length of the pendulum in meters.
            g (float): Acceleration due to gravity in m/s^2.
            dt (float): Time step for simulation in seconds.
            num_steps (int): Total number of simulation steps.
            noise_force_series (numpy.ndarray): External force (noise) time series.
        """
        self.mass = mass
        self.length = length
        self.g = g
        self.dt = dt
        self.step = 0
        self.position_history = []
        self.time_history = []
        self.noise_force_series = self._generate_new_noise(noise_force)
        self.state = [0, 0]  # Initial state [theta, omega]
        self.noise_history = []
        self.applied_force_history = []
        self.gravity_force_history = []
        self.x = self.length * np.sin(self.state[0])

        # Natural angular frequency
        omega_0 = np.sqrt(self.g / self.length)

        # Fudged! Is changeable
        self.damping_coefficient = 1.2

    def reset(self, state=[0, 0]):
        self.step = 0
        self.position_history = []
        self.time_history = []
        self.noise_force_series = self.noise_force_series
        self.state = [0, 0]  # Initial state [theta, omega]
        self.noise_history = []
        self.applied_force_history = []
        self.gravity_force_history = []
        self.x = self.length * np.sin(self.state[0])

    def single_pendulum_dynamics(self, state, t, noise_effect, applied_force):
        """Calculate the derivatives of the pendulum state.

        Args:
            state (numpy.ndarray): Current state [theta, omega].
            t (float): Current time (not used)
            g (float): Acceleration due to gravity.
            length (float): Length of the pendulum.
            noise_effect (float): Current noise (force) value.
            applied_force: Applied force to the pendulum

        Returns:
            numpy.ndarray: Derivatives of the state [dtheta_dt, domega_dt].
        """

        theta, omega = state
        dtheta_dt = omega
        domega_dt = (
            -(self.g / self.length) * np.sin(theta)
            + noise_effect / (self.mass * self.length)
            + applied_force / (self.mass * self.length)
            - self.damping_coefficient * omega / self.mass
        )
        return np.array([dtheta_dt, domega_dt])

    def next_step(self, applied_force=0):
        """Perform the pendulum simulation.

        This method simulates the pendulum motion using the Runge-Kutta 4th order
        method. It updates the state and records the position history at each time step.
        """
        self.step += 1
        # Get the current noise effect
        current_noise_force = self.noise_force_series[
            len(self.position_history) % len(self.noise_force_series)
        ]
        self.noise_history.append(current_noise_force)
        self.applied_force_history.append(applied_force)
        self.gravity_force_history.append(
            -(self.g / self.length) * np.sin(self.state[0])
        )

        # Runge-Kutta 4th order integration
        k1 = self.single_pendulum_dynamics(
            self.state, 0, current_noise_force, applied_force
        )
        k2 = self.single_pendulum_dynamics(
            self.state + self.dt * k1 / 2,
            self.dt / 2,
            current_noise_force,
            applied_force,
        )
        k3 = self.single_pendulum_dynamics(
            self.state + self.dt * k2 / 2,
            self.dt / 2,
            current_noise_force,
            applied_force,
        )
        k4 = self.single_pendulum_dynamics(
            self.state + self.dt * k3, self.dt, current_noise_force, applied_force
        )

        # Update the state
        self.state = self.state + self.dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)

        # Record the position and time
        self.x = self.length * np.sin(self.state[0])
        self.position_history.append(self.x)
        self.time_history.append(self.step * self.dt)

        return self.state
