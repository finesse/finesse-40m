"""Finesse 40m PRMI Model Setup.

This script creates a Finesse model of the 40-meter interferometer configured
for PRMI (Power-Recycled Michelson Interferometer). This script
also provides the lists of photodetectors (pd_list) and PDH signals (pdh_list)
accessible in the model.

Author: Kenny Moc
Email: kmoc@ualberta.ca
"""

from finesse_40m.factory import FortyMeterFactory
import finesse.components as fc
from finesse.detectors import PowerDetector


def make_model():
    """Makes the model.

    Returns:
        model: The configured Finesse model.
    """
    # Initialize the factory and set options
    factory = FortyMeterFactory()
    factory.reset()
    factory.options.LSC.add_output_detectors = True
    factory.options.LSC.add_DOFs = True
    factory.options.LSC.add_ADs = True

    # Build the model
    model = factory.make()

    # PRMI alignment settings
    model.PRM.misaligned = 0
    model.SRM.misaligned = 1
    model.ETMX.misaligned = 1
    model.ITMX.misaligned = 0
    model.ETMY.misaligned = 1
    model.ITMY.misaligned = 0

    # Define the PD ports
    POP = model.BS.p3.o
    REFL = model.Prefl.node
    ASS = model.SRM.p1.i

    # List of detectors to add to the model
    # These are detectors not already built in by default
    detectors = [
        PowerDetector("POP", POP),
        PowerDetector("REFL", REFL),
        PowerDetector("AS_pd", ASS),
        fc.ReadoutRF("REFL22", REFL, f=2 * model.f1, output_detectors=True),
        fc.ReadoutRF("REFL110", REFL, f=2 * model.f2, output_detectors=True),
        fc.ReadoutRF("REFL33", REFL, f=3 * model.f1, output_detectors=True),
        fc.ReadoutRF("REFL165", REFL, f=3 * model.f2, output_detectors=True),
        fc.ReadoutRF("AS11", ASS, f=model.f1, output_detectors=True),
        fc.ReadoutRF("AS22", ASS, f=2 * model.f1, output_detectors=True),
        fc.ReadoutRF("AS110", ASS, f=2 * model.f2, output_detectors=True),
    ]

    # Add the detectors to the model
    model.add(detectors)

    # No HOMs
    model.modes(maxtem=0)

    return model


pdh_list = [
    "POP11_I",
    "POP11_Q",
    "POP22_I",
    "POP22_Q",
    "POP55_I",
    "POP55_Q",
    "POP110_I",
    "POP110_Q",
    "REFL11_I",
    "REFL11_Q",
    "REFL22_I",
    "REFL22_Q",
    "REFL55_I",
    "REFL55_Q",
    "REFL110_I",
    "REFL110_Q",
    "REFL33_I",
    "REFL33_Q",
    "REFL165_I",
    "REFL165_Q",
    "AS11_I",
    "AS11_Q",
    "AS22_I",
    "AS22_Q",
    "AS55_I",
    "AS55_Q",
    "AS110_I",
    "AS110_Q",
]

pd_list = ["POP", "AS_pd", "REFL", "Pinx", "Piny"]
