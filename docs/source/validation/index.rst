.. include:: /defs.hrst
.. _validation:

Validation
-----------
Here you can find validation scripts. Note, that the documentation in this section
is generated automatically from the directory `validation/src/`.

.. toctree::
    :maxdepth: 1
    :name: sec-intro

    beamsizes
    01_QL_simple_michelson
    02_QL_PRFPMI_manual
