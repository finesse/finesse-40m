.. include:: /defs.hrst
.. _getting_started_PRMI:

40m Quantum Limited Sensitivity - PRMI
----------------------------------------------------

In this example, we compute the Quantum Limited sensitivity
for PRMI using real suspensions.

.. jupyter-execute::

    import finesse
    import finesse.analysis.actions as fac
    from finesse.detectors import PowerDetector
    import finesse.components as fc


    import finesse_40m
    from finesse_40m.logging import setup_logger
    setup_logger(context="docs") # For documentation only
    # call without any arguments in your code
    # or, define your own function!

    from finesse_40m.actions import InitialLockPRMI
    from finesse_40m.factory import FortyMeterFactory
    from finesse_40m.locks import (
        add_PRMI_locks, # function to add PRMI locks
        plot_PRMI_error_signals # function to plot PRMI error sigs
    )

    # Functions for checking the IFO state
    from finesse_40m.utils import (
        get_PDs,
        get_readouts,
        fmtpower
    )

    import matplotlib.pyplot as plt
    finesse.init_plotting()
    import pprint

Next we build the model.

.. jupyter-execute::

    factory = FortyMeterFactory()
    factory.reset()

    # Enable detectors for LSC, this is required for locking
    factory.options.LSC.add_output_detectors = True

    # Enable power detectors so we can easily
    # check the IFO state
    factory.options.add_detectors = True

    # We will using a PRMI action defined later
    # so this must be false
    factory.options.LSC.add_locks = False

    # Enable test masses, as defined in the config file
    # NB You should recieve some warnings here informing
    # you that attributes such as sus_Q_pitch are not defined
    # this is okay. These values will be set to reasonable
    # default values corresponding to cylindrical core test masses
    factory.options.suspensions.test_masses = True
    factory.options.suspensions.core_optics = True

    # Print out build options
    print("===== Config =======")
    pprint.PrettyPrinter(indent=4, sort_dicts=True).pprint(
        factory.options.toDict()
    )

    # Build model
    print("===== Building model =======")
    model = factory.make()
    model.modes(modes=None)  # basic maxtem

    print("===== Adding locks and configuring operating point =======")
    add_PRMI_locks(model)
    outputs = model.run(InitialLockPRMI())

    print("===== Writting out data =======")
    for detector in get_PDs(model):
        print(f"{detector} = {fmtpower(outputs['after locks'][detector])}")

    plot_PRMI_error_signals(model)

Check that the error signals and powers are reasonable.

Next we compute the sensitivity for three different input powers

.. jupyter-execute::

    model3 = model.deepcopy()
    model3.parse("""
    # Differentially modulate the arm lengths
    fsig(1)
    sgen darmx ITMX.mech.z
    sgen darmy ITMY.mech.z phase=180

    # Output the full quantum noise limited sensitivity
    qnoised1 NSR_with_RP AS_port.p1 f2 0 nsr=True

    # We could also display the quantum noise and the signal
    # separately by uncommenting these two lines.
    # qnoised noise srm.p2.o
    # pd1 signal srm.p2.o f=fsig
    """)
    x = 10
    y = 100

    ax = fac.Xaxis(model3.darmx.f, 'log', 0.3, 100e3, 100, name='LF')
    out = model3.run(
        fac.Series(
        ax,
        fac.Change({model3.L0.P: x*model.L0.P}),
        ax,
        fac.Change({model3.L0.P: y*model.L0.P}),
        ax
        ),progress_bar=True
    )

    fig, ax1 = plt.subplots(ncols=1,sharey=True,figsize=(7,3))
    ax1.loglog(out[0].x0,2*out[0]['NSR_with_RP'], c='r', ls='--', label=f"Pin = {model.L0.P}")
    ax1.loglog(out[1].x0,2*out[1]['NSR_with_RP'], c='b', ls='--', label=f"Pin = {x*model.L0.P} W")
    ax1.loglog(out[2].x0,2*out[2]['NSR_with_RP'], c='g', ls='--', label=f"Pin = {y*model.L0.P} W")
    ax1.legend()
    ax1.set_ylabel('Sensitivity [m/sqrt(Hz)]')

Here we can see that for the default input power of 800 mW we expect to see
no radiation pressure effects. Instead, our idealised interferometer is limited
by shot noise everywhere. However, after increasing the input power
by 10 and 100 times, we can see a significant radiation pressure effects.

Of course, we have not modelled the angular effects since we have set maxtem
to None. We have also not modelled seismic noise or other key noise sources.
