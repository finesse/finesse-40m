.. include:: ../defs.hrst
.. _examples:

Examples
===============

These pages contain several examples to help you get started with
|Finesse|-40m.

If you are a complete beginner to |Finesse| you may want to check
out the |Finesse| documentation, specifically the `example page on
computing the sensitivity of Advanced LIGO <https://finesse.ifosim.org/docs/latest/examples/09_aligo_sensitivity.html>`_.

You may also wish to see the validation pages too. Those pages contain
models with unit tests and the output of those tests saved into html pages
for ease of viewing.

.. toctree::
    :maxdepth: 1
    :name: sec-examples

    PRMI
    time_domain
    in_air_tables
    mody_input
    custom_suspensions
    prmi_loop_noise
