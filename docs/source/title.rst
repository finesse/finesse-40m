.. include:: /defs.hrst

.. cssclass:: nobg
.. figure:: images/finesse_40_logo_large.png
    :align: center

Overview
========

|Finesse|-40m is a twin to |Finesse|-LIGO. The functionality of |Finesse|-40m and
|Finesse|-LIGO are very similar. The main factory object in
|Finesse|-40m inherits from the |Finesse|-LIGO object. However, most of the
methods are overwritten due to slightly different requirements and architecture.

Nonetheless, |Finesse|-40m has many :ref:`examples` which can be trivially modified to
suit of the |Finesse|-LIGO.

|Finesse|-40m was primarily developed in the summer of 2024 by
`Aaron Goodwin-Jones <https://aaronwjones.com/>`_. It is based on a much earlier
basic parameter file by
`Francisco Salces Carcoba <https://pacosalces.com/>`_ and additional measurements by
Aaron Goodwin-Jones, Francisco Salces Carcoba, Radhika Bhatt, Koji Arai, Kenny Moc,
Jancarlo Sanchez and Rana Adhikari.

The authors thank Daniel Brown and Kevin Kuns for their advice and development of both
|Finesse| and |Finesse|-LIGO.

This codebase replaces the earlier |Finesse|
`2 models <https://git.ligo.org/40m/bhd/-/tree/master/finesse>`_ that had
contributions from many people.
