.. include:: /defs.hrst
.. _parameter_file:

Default Parameter file
========================

This is the default parameter file used by the library.

.. literalinclude:: ./C1.yaml
   :language: yaml
   :linenos:
