from pathlib import Path
import shutil
import re
from sphinx.util import logging
from sphinx.ext import apidoc

logger = logging.getLogger(__name__)

parameter_file_rst = Path("source/parameter_files/index.rst")


def setup(app):
    # This function will be called when the build starts.
    logger.info("Finesse40 Sphinx debugger engaged.")
    app.connect("builder-inited", preproccess)
    app.connect("build-finished", build_finished)


def preproccess(app):
    copy_parameter_file()
    run_apidoc()
    preprocess_validationtest_files()


def copy_parameter_file():
    # Copy across the parameter file. It's a bit easier to debug
    # that a relative import (sphinx fails silently if it can't find the file)
    param_file = Path("../src/finesse_40m/parameter_files/C1.yaml")
    if not param_file.exists():
        logger.exception(f"{param_file}, does not exist")
    if not parameter_file_rst.exists():
        logger.exception(
            f"Parameter file documentation has been (re)moved. "
            "Please update `docs/source/40m_sphinx.py`. "
            f"Expected location: {parameter_file_rst}"
        )
    shutil.copy(Path(param_file), parameter_file_rst.parent / "C1.yaml")


def run_apidoc():
    apidoc.main(
        [
            "-o",
            "source/api",
            "../src/finesse_40m",  # Adjust paths accordingly
            "--force",
            "--separate",
        ]
    )


def preprocess_validationtest_files():
    # Define the source directory relative to conf.py
    source_dir = Path("../validation/src/")

    output_dir = Path("source/validation")

    # Regular expression pattern to match content between the tags
    tag_pattern = re.compile(r"TAG_BEGIN_TEST_ONLY.*?TAG_END_TEST_ONLY", re.DOTALL)

    # Process each file in the source directory
    for filepath in source_dir.glob("*.rst"):

        # Define the new filename with .rst extension
        new_filename = output_dir / filepath.name

        # Read the original file content
        with filepath.open("r", encoding="utf-8") as f:
            content = f.read()

        # Remove content between the tags
        modified_content = re.sub(tag_pattern, "", content)

        if new_filename.exists():
            with new_filename.open("r") as f:
                if f.read() == modified_content:
                    logger.info(f"{new_filename} unchanged. Not overwriting.")
                    continue

        # Write the modified content to the new file
        with new_filename.open("w", encoding="utf-8") as f:
            f.write(modified_content)

        logger.info(f"Processed {filepath} -> {new_filename}")


def check_file(output_file, phrase):
    # Check if the file exists
    if not output_file.exists():
        logger.warning(
            f"File {output_file} not found. File is expected by "
            "`docs/source/40m_sphinx.py` extension."
        )

    # Read the file and search for the keyword
    with output_file.open("r", encoding="utf-8") as f:
        content = f.read()

    # Check for the presence of "length_arm"
    if phrase in content:
        logger.info(f"{phrase} found in {output_file}")
    else:
        logger.exception(f"{phrase} not found in {output_file}")


def build_finished(app, exception):
    # This function will be called when the build finishes.
    # Here, you can check for the file and log relevant information.

    if app.builder.name != "html":
        logger.info("Build is not html, additional checks not enabled.")
    else:
        # Access the build directory
        build_dir = Path(app.outdir)

        if False:
            # Debugging aid only
            logger.info("HTML Build directory contents:")
            for file in build_dir.rglob("*"):
                logger.info(f" - {file.relative_to(build_dir)}")

        check_file(build_dir / "parameter_files" / "index.html", "length_arm")

    if exception:
        logger.error("Build finished with an exception.")
    else:
        logger.info("Build finished successfully.")
