.. include:: /defs.hrst

API Documentation
=================

This reference manual details the modules, classes and functions included in
|Finesse| 40m. The class is the :class:`finesse_40m.factory.base.FortyMeterFactory`
and I strongly recommend reading that before looking at the rest of the API.

.. toctree::
    :maxdepth: 4
    :name: sec-api

    modules
    finesse_40m
