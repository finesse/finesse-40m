.. include:: /defs.hrst
.. _contributors:

About us
========

|Finesse|-40m is a thin wrapper over |Finesse| LIGO. It is being developed
so that BHD commissioning tools developed at the 40m can be applied easily
to LIGO.

The code is mostly adjusted from |Finesse| LIGO source code.

Finesse-40m development team:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. only:: html

  .. cssclass:: avatar
  .. figure:: /images/aaron.png

.. cssclass:: avatar-text
**Aaron Goodwin-Jones**, Caltech
