.. include:: ../defs.hrst
.. _installation:

Installation
---------------

|Finesse|-40m uses a specific branches of |Finesse|-LIGO
therefore is is strongly recommended to **install it in its own
environment**.

To install, first install |Finesse| using conda or mamba.
The packages `pygraphviz` and `python-foton` are only available on conda. However,
they are only required for the documentation.

Using mamba (recommended):

.. code-block:: console

    $ mamba create -n finesse40
    $ conda activate finesse40
    $ mamba install finesse pygraphviz python-foton
    $ git clone git@git.ligo.org:finesse/finesse-40m.git
    $ pip install .

Using conda:

.. code-block:: console

    $ conda create -n finesse40
    $ conda activate finesse40
    $ conda install -c conda-forge finesse pygraphviz python-foton
    $ git clone git@git.ligo.org:finesse/finesse-40m.git
    $ pip install .
