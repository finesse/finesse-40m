import pytest
from unittest.mock import patch
import finesse
from finesse_40m.factory import FortyMeterFactory
from finesse_40m.locks import (
    add_PRMI_locks,
    plot_PRMI_error_signals,
    add_PRMI_LSC_AC_feedback,
    print_AC_loop_info,
)
from finesse_40m.utils import get_PDs, get_readouts, fmtpower


@pytest.fixture
def finesse_model():
    """Fixture to create and return a Finesse model using FortyMeterFactory."""
    factory = FortyMeterFactory()
    factory.reset()

    # Enable necessary options for the tests
    factory.options.LSC.add_output_detectors = True
    factory.options.LSC.close_AC_loops = True
    factory.options.add_detectors = True
    factory.options.suspensions.test_masses = True
    factory.options.suspensions.core_optics = True

    return factory.make(), factory


def test_add_PRMI_locks(finesse_model):
    """Test that PRMI locks are correctly added to the model."""
    add_PRMI_locks(finesse_model[0])

    locks = [lock.name for lock in finesse_model[0].locks]
    assert "MICH_lock" in locks
    assert "PRCL_lock" in locks


def test_plot_PRMI_error_signals(finesse_model):
    """Test that plotting PRMI error signals runs without error."""
    add_PRMI_locks(finesse_model[0])
    # Mock `plt.show()` to prevent actual display during tests
    with patch("matplotlib.pyplot.show"):
        plot_PRMI_error_signals(finesse_model[0])


def test_add_PRMI_LSC_AC_feedback(finesse_model):
    """Test adding PRMI LSC AC feedback loops."""
    model, factory = finesse_model
    add_PRMI_LSC_AC_feedback(model, factory)

    # Check if the input matrix was updated correctly
    assert factory.LSC_input_matrix.MICH["AS55.I"] == 1
    assert factory.LSC_input_matrix.PRCL["REFL11.I"] == 1


def test_print_AC_loop_info(finesse_model, capsys):
    """Test that AC loop info is printed correctly."""
    print_AC_loop_info(finesse_model[1])
    captured = capsys.readouterr()
    assert "===== Local Drives =======" in captured.out
    assert "===== Input Matrix =======" in captured.out
    assert "===== Output Matrix =======" in captured.out
    assert "===== LSC Controller =======" in captured.out


def test_get_PDs(finesse_model):
    """Test that power detectors are correctly identified."""
    detectors = get_PDs(finesse_model[0])
    assert all(
        isinstance(detector, finesse.detectors.PowerDetector) for detector in detectors
    )


def test_get_readouts(finesse_model):
    """Test that RF readouts are correctly retrieved."""
    readouts = get_readouts(finesse_model[0])
    assert all("_I" in name or "_Q" in name for name in readouts)


@pytest.mark.parametrize(
    "input_value, expected_output",
    [
        (1e7, "10.0 MW"),
        (1e3, "1.0 kW"),
        (10, "10.0 W"),
        (0.001, "1.0 mW"),
        (1e-6, "1.0 uW"),
    ],
)
def test_fmtpower(input_value, expected_output):
    """Test that power formatting works as expected."""
    assert fmtpower(input_value) == expected_output
